"""
Contains the hydrogen deuterium exchange rate fitting algorithm.
"""
import numpy as np
import math
import os
from auxillary import PoiBin

########################
########################
## the online version
# def calculate_intrinsic_exchange_rates(sequence_str, Temperature, pH):
#     """
#     calculates the intrinsic h exchange rates based on the amino acid sequence for a polypeptide chain
#     # calculate the instrinsic exchange rate
#     # taken directly from https://gitlab.com/mcpe/psx/blob/master/Code/IntrinsicExchange.py
#     # changed the raw values based on the paper J. Am. Soc. Mass Spectrom. (2018) 29;1936-1939
#     :param sequence: sequence of the protein (needs to include additional nterm and cterm residues as well)
#     :param temp: temperature
#     :param ph: ph
#     :return: list of intrinsic exchange rates for each residue
#     """
#
#     sequence = [x for x in sequence_str]
#
#     # ka, kb, and kw values
#     ka = 10.0 ** 1.62
#     kb = 10.0 ** 10.00  # the old value
#     # kb = 10.0 ** 10.18  # changed this value to the one reported on the paper on JASMS!
#     kw = 10.0 ** -1.5
#
#     # Temperature correction
#     R = 1.987
#     # gabe has the temp correction factor with 278
#     # the excel sheet from the englander lab also has 278.0
#     TemperatureCorrection = (1.0/Temperature - 1.0/278.0) / R
#     # TemperatureCorrection = (1.0 / Temperature - 1.0 / 293.0) / R  # disregarding this temperature correction formula
#
#     # Activation energies (in cal/mol)
#     AcidActivationEnergy = 14000.0
#     BaseActivationEnergy = 17000.0
#     SolventActivationEnergy = 19000.0
#
#     AspActivationEnergy = 1000.0
#     GluActivationEnergy = 1083.0
#     HisActivationEnergy = 7500.0
#
#     # Corrections based on activation energies
#     AcidTemperatureCorrection = math.exp(- TemperatureCorrection * AcidActivationEnergy)
#     BaseTemperatureCorrection = math.exp(- TemperatureCorrection * BaseActivationEnergy)
#     SolventTemperatureCorrection = math.exp(- TemperatureCorrection * SolventActivationEnergy)
#
#     AspTemperatureCorrection = math.exp(- TemperatureCorrection * AspActivationEnergy)
#     GluTemperatureCorrection = math.exp(- TemperatureCorrection * GluActivationEnergy)
#     HisTemperatureCorrection = math.exp(- TemperatureCorrection * HisActivationEnergy)
#
#     # Corrected pH in D2O
#     # pH += 0.4
#
#     # pK-values
#     pKD = 15.05
#     pKAsp = 4.48 * AspTemperatureCorrection
#     pKGlu = 4.93 * GluTemperatureCorrection
#     pKHis = 7.40 * HisTemperatureCorrection
#
#     LambdaProtonatedAcidAsp = math.log(
#         10.0 ** (-0.90 - pH) / (10.0 ** -pKAsp + 10.0 ** -pH) + 10.0 ** (0.90 - pKAsp) / (
#                     10.0 ** -pKAsp + 10.0 ** -pH), 10.0)
#     LambdaProtonatedAcidGlu = math.log(
#         10.0 ** (-0.60 - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (-0.90 - pKGlu) / (
#                     10.0 ** -pKGlu + 10.0 ** -pH), 10.0)
#     LambdaProtonatedAcidHis = math.log(
#         10.0 ** (-0.80 - pH) / (10.0 ** -pKHis + 10.0 ** -pH) + 10.0 ** (0.00 - pKHis) / (
#                     10.0 ** -pKHis + 10.0 ** -pH), 10.0)
#
#     RhoProtonatedAcidAsp = math.log(
#         10.0 ** (-0.12 - pH) / (10.0 ** -pKAsp + 10.0 ** -pH) + 10.0 ** (0.58 - pKAsp) / (
#                     10.0 ** -pKAsp + 10.0 ** -pH), 10.0)
#     RhoProtonatedAcidGlu = math.log(
#         10.0 ** (-0.27 - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (0.31 - pKGlu) / (
#                     10.0 ** -pKGlu + 10.0 ** -pH), 10.0)
#     RhoProtonatedAcidHis = math.log(
#         10.0 ** (-0.51 - pH) / (10.0 ** -pKHis + 10.0 ** -pH) + 10.0 ** (0.00 - pKHis) / (
#                     10.0 ** -pKHis + 10.0 ** -pH), 10.0)
#
#     LambdaProtonatedBaseAsp = math.log(
#         10.0 ** (0.69 - pH) / (10.0 ** -pKAsp + 10.0 ** -pH) + 10.0 ** (0.10 - pKAsp) / (
#                     10.0 ** -pKAsp + 10.0 ** -pH), 10.0)
#     LambdaProtonatedBaseGlu = math.log(
#         10.0 ** (0.24 - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (-0.11 - pKGlu) / (
#                     10.0 ** -pKGlu + 10.0 ** -pH), 10.0)
#     LambdaProtonatedBaseHis = math.log(
#         10.0 ** (0.80 - pH) / (10.0 ** -pKHis + 10.0 ** -pH) + 10.0 ** (-0.10 - pKHis) / (
#                     10.0 ** -pKHis + 10.0 ** -pH), 10.0)
#
#     RhoProtonatedBaseAsp = math.log(
#         10.0 ** (0.60 - pH) / (10.0 ** -pKAsp + 10.0 ** -pH) + 10.0 ** (-0.18 - pKAsp) / (
#                     10.0 ** -pKAsp + 10.0 ** -pH), 10.0)
#     RhoProtonatedBaseGlu = math.log(
#         10.0 ** (0.39 - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (-0.15 - pKGlu) / (
#                     10.0 ** -pKGlu + 10.0 ** -pH), 10.0)
#     RhoProtonatedBaseHis = math.log(
#         10.0 ** (0.83 - pH) / (10.0 ** -pKHis + 10.0 ** -pH) + 10.0 ** (0.14 - pKHis) / (
#                     10.0 ** -pKHis + 10.0 ** -pH), 10.0)
#
#     # Termini
#     RhoAcidNTerm = -1.32
#     LambdaAcidCTerm = math.log(10.0 ** (0.05 - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (0.96 - pKGlu) / (
#                 10.0 ** -pKGlu + 10.0 ** -pH), 10.0)
#
#     RhoBaseNTerm = 1.62
#     LambdaBaseCTerm = -1.80
#
#     # Ion concentrations
#     DIonConc = 10.0 ** -pH
#     ODIonConc = 10.0 ** (pH - pKD)
#
#     # Dictionary for acid values (format is (lambda, rho))
#     MilneAcid = {}
#
#     MilneAcid["NTerminal"] = (None, RhoAcidNTerm)
#     MilneAcid["CTerminal"] = (LambdaAcidCTerm, None)
#
#     MilneAcid["A"] = (0.00, 0.00)
#     MilneAcid["C"] = (-0.54, -0.46)
#     MilneAcid["C2"] = (-0.74, -0.58)
#     MilneAcid["D0"] = (0.90, 0.58)  # added this item from the JASMS paper
#     MilneAcid["D"] = (LambdaProtonatedAcidAsp, RhoProtonatedAcidAsp)
#     MilneAcid["D+"] = (-0.90, -0.12)
#     MilneAcid["E0"] = (-0.90, 0.31)  # added this item according to the JASMS paper
#     MilneAcid["E"] = (LambdaProtonatedAcidGlu, RhoProtonatedAcidGlu)
#     MilneAcid["E+"] = (-0.60, -0.27)
#     MilneAcid["F"] = (-0.52, -0.43)
#     MilneAcid["G"] = (-0.22, 0.22)
#     MilneAcid["H0"] = [0.00, 0.00]  # added this item according to the JASMS paper
#     MilneAcid["H"] = (LambdaProtonatedAcidHis, RhoProtonatedAcidHis)
#     MilneAcid["H+"] = (-0.80, -0.51)  # added this item according to the JASMS paper
#     MilneAcid["I"] = (-0.91, -0.59)
#     MilneAcid["K"] = (-0.56, -0.29)
#     MilneAcid["L"] = (-0.57, -0.13)
#     MilneAcid["M"] = (-0.64, -0.28)
#     MilneAcid["N"] = (-0.58, -0.13)
#     MilneAcid["P"] = (0.00, -0.19)
#     MilneAcid["Pc"] = (0.00, -0.85)
#     MilneAcid["Q"] = (-0.47, -0.27)
#     MilneAcid["R"] = (-0.59, -0.32)
#     MilneAcid["S"] = (-0.44, -0.39)
#     MilneAcid["T"] = (-0.79, -0.47)
#     MilneAcid["V"] = (-0.74, -0.30)
#     MilneAcid["W"] = (-0.40, -0.44)
#     MilneAcid["Y"] = (-0.41, -0.37)
#
#     # Dictionary for base values (format is (lambda, rho))
#     MilneBase = {}
#
#     MilneBase["NTerminal"] = (None, RhoBaseNTerm)
#     MilneBase["CTerminal"] = (LambdaBaseCTerm, None)
#
#     MilneBase["A"] = (0.00, 0.00)
#     MilneBase["C"] = (0.62, 0.55)
#     MilneBase["C2"] = (0.55, 0.46)
#     MilneBase['D0'] = (0.10, -0.18)  # added this item according to the JASMS paper
#     MilneBase["D"] = (LambdaProtonatedBaseAsp, RhoProtonatedBaseAsp)
#     MilneBase["D+"] = (0.69, 0.60)
#     MilneBase["E0"] = (-0.11, -0.15)  # added this item according to the JASMS paper
#     MilneBase["E"] = (LambdaProtonatedBaseGlu, RhoProtonatedBaseGlu)
#     MilneBase["E+"] = (0.24, 0.39)
#     MilneBase["F"] = (-0.24, 0.06)
#     MilneBase["G"] = (0.27, 0.17)  # old value
#     # MilneBase["G"] = (-0.03, 0.17)  # changed this value according to the JASMS paper
#     MilneBase["H0"] = (-0.10, 0.14)  # added this item according to the JASMS paper
#     MilneBase["H"] = (LambdaProtonatedBaseHis, RhoProtonatedBaseHis)
#     MilneBase["H+"] = (0.80, 0.83)  # added this item according to the JASMS paper
#     MilneBase["I"] = (-0.73, -0.23)
#     MilneBase["K"] = (-0.04, 0.12)
#     MilneBase["L"] = (-0.58, -0.21)
#     MilneBase["M"] = (-0.01, 0.11)
#     MilneBase["N"] = (0.49, 0.32)
#     MilneBase["P"] = (0.00, -0.24)
#     MilneBase["Pc"] = (0.00, 0.60)
#     MilneBase["Q"] = (0.06, 0.20)
#     MilneBase["R"] = (0.08, 0.22)
#     MilneBase["S"] = (0.37, 0.30)
#     MilneBase["T"] = (-0.07, 0.20)
#     MilneBase["V"] = (-0.70, -0.14)
#     MilneBase["W"] = (-0.41, -0.11)
#     MilneBase["Y"] = (-0.27, 0.05)
#
#     # Default values
#     MilneAcid["?"] = (0.00, 0.00)
#     MilneBase["?"] = (0.00, 0.00)
#
#     # Loop over the chain
#     IntrinsicEnchangeRates = [0.0]
#     sequence.insert(0, "NTerminal")
#     sequence.append("CTerminal")
#
#     # Account for middle residues
#     for i in range(2, len(sequence) - 1):
#         Residue = sequence[i]
#
#         if Residue in ("P", "Pc"):
#             IntrinsicEnchangeRates.append(0.0)
#
#         else:
#             # Identify neighbors
#             LeftResidue = sequence[i - 1]
#             RightResidue = sequence[i + 1]
#
#             if RightResidue == "CTerminal":
#                 Fa = 10.0 ** (MilneAcid[LeftResidue][1] + MilneAcid[Residue][0] + MilneAcid["CTerminal"][0])
#                 Fb = 10.0 ** (MilneBase[LeftResidue][1] + MilneBase[Residue][0] + MilneBase["CTerminal"][0])
#
#             elif i == 2:
#                 Fa = 10.0 ** (MilneAcid["NTerminal"][1] + MilneAcid[LeftResidue][1] + MilneAcid[Residue][0])
#                 Fb = 10.0 ** (MilneBase["NTerminal"][1] + MilneBase[LeftResidue][1] + MilneBase[Residue][0])
#
#             else:
#                 Fa = 10.0 ** (MilneAcid[LeftResidue][1] + MilneAcid[Residue][0])
#                 Fb = 10.0 ** (MilneBase[LeftResidue][1] + MilneBase[Residue][0])
#
#             # Contributions from acid, base, and water
#             kaT = Fa * ka * AcidTemperatureCorrection * DIonConc
#             kbT = Fb * kb * BaseTemperatureCorrection * ODIonConc
#             kwT = Fb * kw * SolventTemperatureCorrection
#
#             # Collect exchange rates
#             IntrinsicExchangeRate = kaT + kbT + kwT
#
#             # Construct list
#             IntrinsicEnchangeRates.append(IntrinsicExchangeRate)
#
#     # Rescale from 1/min to 1/s
#     IntrinsicEnchangeRates = [IntrinsicEnchangeRates[i] / 60.0 for i in range(len(IntrinsicEnchangeRates))]
#
#     return IntrinsicEnchangeRates
###########################################
###########################################


def calculate_intrinsic_exchange_rates_suggie(sequence_str, Temperature, pH, nterm_mode='NT', cterm_mode='CT'):
    """
    calculates the intrinsic h exchange rates based on the amino acid sequence for a polypeptide chain
    # calculate the instrinsic exchange rate
    # taken directly from https://gitlab.com/mcpe/psx/blob/master/Code/IntrinsicExchange.py
    # changed the raw values based on the paper J. Am. Soc. Mass Spectrom. (2018) 29;1936-1939
    :param sequence: sequence of the protein (needs to include additional nterm and cterm residues as well)
    :param temp: temperature
    :param ph: ph
    :return: list of intrinsic exchange rates for each residue
    """

    sequence = [x for x in sequence_str]
    sequence.insert(0, nterm_mode)
    sequence.append(cterm_mode)

    # ka, kb, and kw values
    ka = (10.0 ** 1.62)/60
    kb = (10.0 ** 10.18)/60  # changed this value to the one reported on the paper on JASMS! (10.00 init)
    kw = (10.0 ** -1.5)/60

    # Temperature correction
    R = 1.987
    # gabe has the temp correction factor with 278
    # the excel sheet from the englander lab also has 278.0
    TemperatureCorrection = (1.0/Temperature - 1.0/278.0) / R
    Temperature_Corr_2 = (1.0/Temperature - 1.0/293.0) / R
    # TemperatureCorrection = (1.0 / Temperature - 1.0 / 293.0) / R  # disregarding this temperature correction formula

    # Activation energies (in cal/mol)
    AcidActivationEnergy = 14000.0
    BaseActivationEnergy = 17000.0
    SolventActivationEnergy = 19000.0

    AspActivationEnergy = 1000.0
    GluActivationEnergy = 1083.0
    HisActivationEnergy = 7500.0

    # Corrections based on activation energies
    # AcidTemperatureCorrection = math.exp(- TemperatureCorrection * AcidActivationEnergy)
    # BaseTemperatureCorrection = math.exp(- TemperatureCorrection * BaseActivationEnergy)
    # SolventTemperatureCorrection = math.exp(- TemperatureCorrection * SolventActivationEnergy)

    AspTemperatureCorrection = math.exp(- TemperatureCorrection * AspActivationEnergy)
    GluTemperatureCorrection = math.exp(- TemperatureCorrection * GluActivationEnergy)
    HisTemperatureCorrection = math.exp(- TemperatureCorrection * HisActivationEnergy)

    # Corrected pH in D2O
    # pH += 0.4

    # pK-values
    pKD = 15.05
    # pKAsp = 4.48 * AspTemperatureCorrection
    pKAsp = math.log10(10**(-1*4.48)*AspTemperatureCorrection)*-1
    pKGlu = math.log10(10**(-1*4.93)*GluTemperatureCorrection)*-1
    pKHis = math.log10(10**(-1*7.42)*HisTemperatureCorrection)*-1


    # create dictionary to store the amino acids L and R reference values for both acid and base

    MilneAcid = {}

    # MilneAcid["NTerminal"] = (None, RhoAcidNTerm)
    # MilneAcid["CTerminal"] = (LambdaAcidCTerm, None)

    MilneAcid["A"] = (0.00, 0.00)
    MilneAcid["C"] = (-0.54, -0.46)
    MilneAcid["C2"] = (-0.74, -0.58)
    MilneAcid["D0"] = (0.90, 0.58)  # added this item from the JASMS paper
    MilneAcid["D+"] = (-0.90, -0.12)
    MilneAcid["E0"] = (-0.90, 0.31)  # added this item according to the JASMS paper
    MilneAcid["E+"] = (-0.60, -0.27)
    MilneAcid["F"] = (-0.52, -0.43)
    MilneAcid["G"] = (-0.22, 0.22)
    MilneAcid["H0"] = [0.00, 0.00]  # added this item according to the JASMS paper
    MilneAcid["H+"] = (-0.80, -0.51)  # added this item according to the JASMS paper
    MilneAcid["I"] = (-0.91, -0.59)
    MilneAcid["K"] = (-0.56, -0.29)
    MilneAcid["L"] = (-0.57, -0.13)
    MilneAcid["M"] = (-0.64, -0.28)
    MilneAcid["N"] = (-0.58, -0.13)
    MilneAcid["P"] = (0.00, -0.19)
    MilneAcid["Pc"] = (0.00, -0.85)
    MilneAcid["Q"] = (-0.47, -0.27)
    MilneAcid["R"] = (-0.59, -0.32)
    MilneAcid["S"] = (-0.44, -0.39)
    MilneAcid["T"] = (-0.79, -0.47)
    MilneAcid["V"] = (-0.74, -0.30)
    MilneAcid["W"] = (-0.40, -0.44)
    MilneAcid["Y"] = (-0.41, -0.37)

    # Dictionary for base values (format is (lambda, rho))
    MilneBase = {}

    # MilneBase["NTerminal"] = (None, RhoBaseNTerm)
    # MilneBase["CTerminal"] = (LambdaBaseCTerm, None)

    MilneBase["A"] = (0.00, 0.00)
    MilneBase["C"] = (0.62, 0.55)
    MilneBase["C2"] = (0.55, 0.46)
    MilneBase['D0'] = (0.10, -0.18)  # added this item according to the JASMS paper
    MilneBase["D+"] = (0.69, 0.60)
    MilneBase["E0"] = (-0.11, -0.15)  # added this item according to the JASMS paper
    MilneBase["E+"] = (0.24, 0.39)
    MilneBase["F"] = (-0.24, 0.06)
    MilneBase["G"] = (0.27, 0.17)  # old value
    # MilneBase["G"] = (-0.03, 0.17)  # changed this value according to the JASMS paper
    MilneBase["H0"] = (-0.10, 0.14)  # added this item according to the JASMS paper
    MilneBase["H+"] = (0.80, 0.83)  # added this item according to the JASMS paper
    MilneBase["I"] = (-0.73, -0.23)
    MilneBase["K"] = (-0.04, 0.12)
    MilneBase["L"] = (-0.58, -0.21)
    MilneBase["M"] = (-0.01, 0.11)
    MilneBase["N"] = (0.49, 0.32)
    MilneBase["P"] = (0.00, -0.24)
    MilneBase["Pc"] = (0.00, 0.60)
    MilneBase["Q"] = (0.06, 0.20)
    MilneBase["R"] = (0.08, 0.22)
    MilneBase["S"] = (0.37, 0.30)
    MilneBase["T"] = (-0.07, 0.20)
    MilneBase["V"] = (-0.70, -0.14)
    MilneBase["W"] = (-0.41, -0.11)
    MilneBase["Y"] = (-0.27, 0.05)

    # Default values
    MilneAcid["?"] = (0.00, 0.00)
    MilneBase["?"] = (0.00, 0.00)

    LambdaProtonatedAcidAsp = math.log10(
        10.0 ** (MilneAcid['D+'][0] - pH) / (10.0 ** -pKAsp + 10.0 ** -pH) + 10.0 ** (MilneAcid['D0'][0] - pKAsp) / (
                    10.0 ** -pKAsp + 10.0 ** -pH))
    LambdaProtonatedAcidGlu = math.log10(
        10.0 ** (MilneAcid['E+'][0] - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (MilneAcid['E0'][0] - pKGlu) / (
                    10.0 ** -pKGlu + 10.0 ** -pH))
    LambdaProtonatedAcidHis = math.log10(
        10.0 ** (MilneAcid['H+'][0] - pH) / (10.0 ** -pKHis + 10.0 ** -pH) + 10.0 ** (MilneAcid['H0'][0] - pKHis) / (
                    10.0 ** -pKHis + 10.0 ** -pH))

    RhoProtonatedAcidAsp = math.log10(
        10.0 ** (MilneAcid['D+'][1] - pH) / (10.0 ** -pKAsp + 10.0 ** -pH) + 10.0 ** (MilneAcid['D0'][1] - pKAsp) / (
                    10.0 ** -pKAsp + 10.0 ** -pH))
    RhoProtonatedAcidGlu = math.log10(
        10.0 ** (MilneAcid['E+'][1] - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (MilneAcid['E0'][1] - pKGlu) / (
                    10.0 ** -pKGlu + 10.0 ** -pH))
    RhoProtonatedAcidHis = math.log10(
        10.0 ** (MilneAcid['H+'][1] - pH) / (10.0 ** -pKHis + 10.0 ** -pH) + 10.0 ** (MilneAcid['H0'][1] - pKHis) / (
                    10.0 ** -pKHis + 10.0 ** -pH))

    LambdaProtonatedBaseAsp = math.log10(
        10.0 ** (MilneBase['D+'][0] - pH) / (10.0 ** -pKAsp + 10.0 ** -pH) + 10.0 ** (MilneBase['D0'][0] - pKAsp) / (
                    10.0 ** -pKAsp + 10.0 ** -pH))
    LambdaProtonatedBaseGlu = math.log10(
        10.0 ** (MilneBase['E+'][0] - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (MilneBase['E0'][0] - pKGlu) / (
                    10.0 ** -pKGlu + 10.0 ** -pH))
    LambdaProtonatedBaseHis = math.log10(
        10.0 ** (MilneBase['H+'][0] - pH) / (10.0 ** -pKHis + 10.0 ** -pH) + 10.0 ** (MilneBase['H0'][0] - pKHis) / (
                    10.0 ** -pKHis + 10.0 ** -pH))

    RhoProtonatedBaseAsp = math.log10(
        10.0 ** (MilneBase['D+'][1] - pH) / (10.0 ** -pKAsp + 10.0 ** -pH) + 10.0 ** (MilneBase['D0'][1] - pKAsp) / (
                    10.0 ** -pKAsp + 10.0 ** -pH))
    RhoProtonatedBaseGlu = math.log10(
        10.0 ** (MilneBase['E+'][1] - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (MilneBase['E0'][1] - pKGlu) / (
                    10.0 ** -pKGlu + 10.0 ** -pH))
    RhoProtonatedBaseHis = math.log10(
        10.0 ** (MilneBase['H+'][1] - pH) / (10.0 ** -pKHis + 10.0 ** -pH) + 10.0 ** (MilneBase['H0'][1] - pKHis) / (
                    10.0 ** -pKHis + 10.0 ** -pH))

    MilneAcid["D"] = (LambdaProtonatedAcidAsp, RhoProtonatedAcidAsp)
    MilneAcid["E"] = (LambdaProtonatedAcidGlu, RhoProtonatedAcidGlu)
    MilneAcid["H"] = (LambdaProtonatedAcidHis, RhoProtonatedAcidHis)

    MilneBase["D"] = (LambdaProtonatedBaseAsp, RhoProtonatedBaseAsp)
    MilneBase["E"] = (LambdaProtonatedBaseGlu, RhoProtonatedBaseGlu)
    MilneBase["H"] = (LambdaProtonatedBaseHis, RhoProtonatedBaseHis)

    # Termini
    RhoAcidNTerm = -1.32
    LambdaAcidCTerm = math.log10(10.0 ** (0.05 - pH) / (10.0 ** -pKGlu + 10.0 ** -pH) + 10.0 ** (0.96 - pKGlu) / (
                10.0 ** -pKGlu + 10.0 ** -pH))

    RhoBaseNTerm = 1.62
    LambdaBaseCTerm = -1.80

    MilneAcid["NT"] = (None, RhoAcidNTerm)
    MilneAcid["CT"] = (LambdaAcidCTerm, None)

    MilneBase["NT"] = (None, RhoBaseNTerm)
    MilneBase["CT"] = (LambdaBaseCTerm, None)

    # N terminal methylation
    LambdaAcidNMe = math.log10(135.5/(ka*60))
    LambdaBaseNMe = math.log10(2970000000/(kb*60))

    MilneAcid["NMe"] = (LambdaAcidNMe, None)
    MilneBase["NMe"] = (LambdaBaseNMe, None)

    # Acetylation
    MilneAcid["Ac"] = (None, 0.29)
    MilneBase["Ac"] = (None, -0.20)

    # Ion concentrations
    DIonConc = 10.0 ** -pH
    ODIonConc = 10.0 ** (pH - pKD)

    # Loop over the chain starting with 0 for initial residue
    IntrinsicEnchangeRates = [0.0]
    IntrinsicEnchangeRates_min = [0.0]

    # Account for middle residues
    for i in range(2, len(sequence) - 1):
        Residue = sequence[i]

        if Residue in ("P", "Pc"):
            IntrinsicEnchangeRates.append(0.0)

        else:
            # Identify neighbors
            LeftResidue = sequence[i - 1]
            RightResidue = sequence[i + 1]

            if RightResidue == "CT":
                Fa = 10.0 ** (MilneAcid[LeftResidue][1] + MilneAcid[Residue][0] + MilneAcid["CT"][0])
                Fb = 10.0 ** (MilneBase[LeftResidue][1] + MilneBase[Residue][0] + MilneBase["CT"][0])

            elif i == 2:
                Fa = 10.0 ** (MilneAcid["NT"][1] + MilneAcid[LeftResidue][1] + MilneAcid[Residue][0])
                Fb = 10.0 ** (MilneBase["NT"][1] + MilneBase[LeftResidue][1] + MilneBase[Residue][0])

            else:
                Fa = 10.0 ** (MilneAcid[LeftResidue][1] + MilneAcid[Residue][0])
                Fb = 10.0 ** (MilneBase[LeftResidue][1] + MilneBase[Residue][0])

            # Contributions from acid, base, and water

            Fta = math.exp(-1*AcidActivationEnergy*Temperature_Corr_2)
            Ftb = math.exp(-1*BaseActivationEnergy*Temperature_Corr_2)
            Ftw = math.exp(-1*SolventActivationEnergy*Temperature_Corr_2)

            kaT = Fa * ka * DIonConc * Fta
            kbT = Fb * kb * ODIonConc * Ftb
            kwT = Fb * kw * Ftw
            # kaT = Fa * ka * AcidTemperatureCorrection * DIonConc
            # kbT = Fb * kb * BaseTemperatureCorrection * ODIonConc
            # kwT = Fb * kw * SolventTemperatureCorrection

            # Collect exchange rates
            IntrinsicExchangeRate = kaT + kbT + kwT

            # To compare with the excel sheet from Englander Lab
            IntrinsicExchangeRate_min = IntrinsicExchangeRate * 60

            # Construct list
            IntrinsicEnchangeRates.append(IntrinsicExchangeRate)

            # To compare with the excel sheet from Englander lab
            IntrinsicEnchangeRates_min.append(IntrinsicExchangeRate_min)


    IntrinsicEnchangeRates = np.array(IntrinsicEnchangeRates)
    IntrinsicEnchangeRates_min = np.array(IntrinsicEnchangeRates_min)

    return IntrinsicEnchangeRates


def calc_hx_prob_no_backexchange(timepoints, rate_constant):
    """
    calculates the exchange probability for each time point given in timepoints given the rate constant(s).
    :param timepoints: array of time points (seconds)
    :param rate_constant: can be a single rate constant or an array of rate constants of the same size as timepoints
    :return: array of excchange probabilities
    """
    prob = np.array(1.0 - np.exp(-rate_constant * timepoints))
    return prob


def cal_hx_prob_with_backexchange(timepoints, rate_constant, backexchange=0.9, d2o_purity=0.95, d2o_fraction=0.9):
    """
    calculate the exchange probability for each time point given the rate constant, backexchange, and d20 purity and
    fraction
    :param timepoints: array of time points (seconds)
    :param rate_constant: array of rate constants or a single rate
    :param backexchange: backexchange rate
    :param d2o_purity: purity of d2o
    :param d2o_fraction:
    :return: exchange probabilities
    """
    prob = (1.0 - np.exp(-rate_constant * timepoints)) * (d2o_fraction * d2o_purity * backexchange)
    return prob


def cal_hx_prob_with_backexchange_weighted(timepoints, rate_constant, weight_backexchange, backexchange=0.001,
                                           d2o_purity=0.95, d2o_fraction=0.9):
    """
    calculate the exchange probability for each time point given the rate constant, backexchange, and d2o purity and
    fraction.
    In this case, backexchange has a weight associated with it so that can be used as a fitting param
    :param timepoints: array of time points (seconds)
    :param rate_constant: array of rate constants
    :param weight_backexchange: weight on backexchange
    :param backexchange: back excchange rate
    :param d2o_purity: purity of d2o
    :param d2o_fraction: fraction of d2o
    :return: exchange probabilities
    """
    prob = (1.0 - np.exp(-rate_constant * timepoints)) * (np.exp(-weight_backexchange * backexchange)) * (
                d2o_fraction * d2o_purity)
    return prob


def hx_rate_with_free_energy(timepoints, intrinsic_rates, free_energy, backexchange=0.9, temp=295, d2o_purity=0.95,
                             d2o_fraction=0.9, r_constant=8.314):
    """
    determine the observed sum hx rate given the intrinsic rates,
    :param timepoints: time points array (seconds)
    :param intrinsic_rates: intrinsic rates (sec-1)
    :param free_energy: free energies
    :param backexchange: backexchange rate
    :param temp: temperature
    :param d2o_fraction: fraction of d2o
    :param d2o_purity: purity of d2o
    :param r_constant: the constant R = 8.314
    :return: a sum rate
    """
    if isinstance(free_energy, list):
        free_energy = np.array(free_energy)

    fraction = np.exp(-free_energy / (r_constant * temp)) / (1 + np.exp(-free_energy / (r_constant * temp)))
    obs_rates = intrinsic_rates * fraction
    sum_rate = 0
    for num in range(len(obs_rates)):
        sum_rate += calc_hx_prob_no_backexchange(timepoints, obs_rates[num])
    sum_rate = sum_rate * backexchange * d2o_purity * d2o_fraction
    return sum_rate


def hx_rates_from_free_energies(intrinsic_rates, free_energies, temp=295, r_constant=8.314):
    """
    calculates the hx rates with free energies
    :param intrinsic_rates: intrinsic rates (sec-1)
    :param free_energies: free energies
    :param temp: temperature
    :param r_constant: the constant R = 8.314
    :return: array of observed rates
    """
    free_energies_array = np.array(free_energies, dtype=np.float)
    # get a boolean array where if free energy is nan it defaults to false
    free_energies_indexing = np.logical_not(np.isnan(free_energies_array))

    out_array = np.zeros(np.shape(free_energies_array))

    fractions = np.exp(-free_energies_array / (r_constant * temp)) / (
                1 + np.exp(-free_energies_array / (r_constant * temp)))
    out_array[free_energies_indexing] = intrinsic_rates[free_energies_indexing] * fractions[free_energies_indexing]

    return out_array


def free_energies_from_rates(intrinsic_rates, measured_rates, fast_factor=2.0, min_free_energy=-10, r_constant=8.314,
                             temp=295, fill_blank=False):
    """

    :param intrinsic_rates: intrinsic rates (sec-1). Can use approx rates if intrinsic rates is not available
    :param measured_rates: measured rates (sec-1)
    :param fast_factor: factor to apply to get highest rates if meas rates is blank
    :param min_free_energy: minimum free energy
    :param fill_blank: bool. Fill blank if obs rates is nan, fills with max_rate * fast_factor value
    :return: array of free energies calculated from the rates
    """
    obs_rates = np.array(measured_rates, dtype=np.float)
    intrinsic_rates = np.array(intrinsic_rates, dtype=np.float)
    if fill_blank:
        max_rate = max(obs_rates[np.isfinite(obs_rates)])
        obs_rates[np.isnan(obs_rates)] = max_rate * fast_factor

    obs_rates_index = np.logical_not(np.isnan(obs_rates))
    obs_rates_sorted = obs_rates[obs_rates_index]
    kop = obs_rates_sorted / (intrinsic_rates[obs_rates_index] - obs_rates_sorted)

    free_energy_out = np.empty(np.shape(obs_rates_sorted))
    free_energies = -r_constant * temp * np.log(kop)
    free_energies[np.isnan(free_energies)] = min_free_energy
    free_energies[free_energies < min_free_energy] = min_free_energy
    free_energy_out[obs_rates_index] = free_energies
    free_energy_out[intrinsic_rates == 0] = np.nan

    return free_energy_out


# todo: check to see whether the pmf hx probs needs to be its own function or can be incorporated into calculating the istope distribution
def hx_rates_probability_distribution_with_free_energies(timepoints, rates, free_energies, temp=295, r_constant=8.314, backexchange=0.9,
                             d2o_fraction=0.95, d2o_purity=0.9):
    """
    returns the Poisson Binomial (PoiBin) distribution probability mass function (pmf) of the hx probabilities given the
    time points, measured rates, free energies, and back exchange
    :param timepoints: time points in seconds
    :param rates: measured rates
    :param free_energies: free energies
    :param temp: temperature in kelvin
    :param r_constant: R constant
    :param backexchange: backexchange rate
    :param d2o_fraction: fraction of d2o
    :param d2o_purity: purity of d2o
    :return: PoiBin pmf of hx probabilities
    """
    if type(free_energies) == type(list):
        free_energies = np.array(free_energies)

    fraction = np.exp(-free_energies / (r_constant * temp)) / (1 + np.exp(-free_energies / (r_constant * temp)))
    hx_probabs = cal_hx_prob_with_backexchange(timepoints, rates * fraction, backexchange=backexchange,
                                               d2o_fraction=d2o_fraction, d2o_purity=d2o_purity)
    hx_probabs[hx_probabs != hx_probabs] = 0
    pmf_hx_probabs = PoiBin(hx_probabs).get_pmf_xi()
    return pmf_hx_probabs


# todo: check to see whether the pmf hx probs needs to be its own function or can be incorporated into calculating the istope distribution
def hx_rates_probability_distribution(timepoints, rates, backexchange=0.9, d2o_fraction=0.9, d2o_purity=0.95):
    """
    returns the Poisson Binomial (PoiBin) distribution probability mass function (pmf) of the hx probabilities given the
    time points, measured rates, and backexchange
    :param timepoints: time points array in seconds
    :param rates: measured rates
    :param backexchange: backexchange rate
    :param temp: temperature in kelvin
    :param d2o_fraction: fraction of d2o
    :param d2o_purity: purity of d2o
    :return: PoiBin pmf of hx probabilities
    """
    hx_probabs = cal_hx_prob_with_backexchange(timepoints=timepoints, rate_constant=np.exp(rates), backexchange=backexchange,
                                               d2o_purity=d2o_purity, d2o_fraction=d2o_fraction)
    pmf_hx_probabs = PoiBin(hx_probabs)
    # pmf_hx_probabs = PoiBin(hx_probabs).get_pmf_xi()
    return pmf_hx_probabs


def isotope_dist_from_PoiBin_calc_with_hxrates_free_energy(isotope_dist, timepoints, rates, free_energies, r_constant,
                                                   num_bins=50, backexchange=0.9, d2o_fraction=0.9, d2o_purity=0.95,
                                                   temp=295):
    """
    calculates the istope distribution based on PoiBin function given the hx rates and free energies
    :param isotope_dist: isotope distribution
    :param timepoints: time points array in seconds
    :param rates: measured rates array
    :param free_energies: free energies array
    :param num_bins: number of bins o
    :param backexchange:
    :return:
    """
    pmf_hx_prob = hx_rates_probability_distribution_with_free_energies(timepoints=timepoints, rates=rates,
                                                                       free_energies=free_energies,
                                                                       temp=temp, r_constant=r_constant,
                                                                       backexchange=backexchange,
                                                                       d2o_purity=d2o_purity,
                                                                       d2o_fraction=d2o_fraction)
    isotope_dist_poibin_convol = np.convolve(pmf_hx_prob, isotope_dist)
    isotope_dist_poibin_convol_norm = isotope_dist_poibin_convol[:num_bins]/max(isotope_dist_poibin_convol[:num_bins])
    return isotope_dist_poibin_convol_norm


def isotope_dist_from_PoiBin_calc_with_hx_rates(isotope_dist, timepoints, rates, num_bins=50, backexchange=0.9,
                                                d2o_fraction=0.9, d2o_purity=0.9):
    """
    returns the convolved isotopic distribution from the pfm of hx rates probabilities
    :param isotope_dist: isotope distribution
    :param timepoints: time points array in seconds
    :param rates: measured rates array
    :param num_bins: number of bins
    :param backexchange: back exchange rate as above
    :param d2o_fraction: fraction of d2o
    :param d2o_purity: purity of d2o
    :return: isotope distribution
    """
    pmf_hx_prob = hx_rates_probability_distribution(timepoints=timepoints, rates=rates, backexchange=backexchange,
                                                    d2o_fraction=d2o_fraction, d2o_purity=d2o_purity)
    isotope_dist_poibin_convol = np.convolve(pmf_hx_prob, isotope_dist)
    isotope_dist_poibin_convol_norm = isotope_dist_poibin_convol[:num_bins]/max(isotope_dist_poibin_convol[:num_bins])
    return isotope_dist_poibin_convol_norm


if __name__=='__main__':

    # tests
    # sequence = 'DLRKNNKELWLLRKNN'
    # sequence = 'AAAWHDEAA'
    import Bio.PDB
    dirpath = "/Users/smd4193/Documents/hx_ratefit_gabe"
    pdb_path = os.path.join(dirpath, "HHH_rd4_0997.pdb")
    Nterm = 'HM'
    Cterm = 'GSSGSSGNS'
    structure = Bio.PDB.PDBParser(QUIET=True).get_structure(pdb_path, pdb_path)
    residues = [res for res in structure[0].get_residues()]
    sequence = ''.join([Bio.PDB.Polypeptide.three_to_one(aa.get_resname()) for aa in residues])
    sequence = Nterm + sequence + Cterm

    timepoints = [5, 10, 15, 20, 25, 30]

    pD = 6.15
    temp = 295
    intrinsic_rates = calculate_intrinsic_exchange_rates_suggie(sequence, Temperature=temp, pH=pD)

    # intrinsic_rates = [1.28, 1.36, 1.4068, 1.425, 1.446, 1.452]
    measured_rates = [0.6385, 0.6789, 0.7023, 0.7123, 0.7229, 0.7259]
    free_energies = free_energies_from_rates(intrinsic_rates, measured_rates)
    hx_rates_probability_distribution_with_free_energies(timepoints, measured_rates, free_energies)
    isotope_distribution = [0.001, 0.005, 0.101, 0.235, 0.567, 0.689, 0.889, 0.998, 0.976, 0.876, 0.688, 0.565, 0.243,
                            0.100, 0.008, 0.002]
    isotope_distribution = np.array(isotope_distribution)
    out = isotope_dist_from_PoiBin_calc_with_hxrates_free_energy(isotope_distribution, timepoints, measured_rates, free_energies,
                                                         r_constant=8.314)


    # print(out)
    print('heho')