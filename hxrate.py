# do all the hx rate fitting to the data here

import os
import pickle
import numpy as np
from scipy.special import expit
from sklearn.metrics import mean_squared_error
from scipy.optimize import fmin_powell, basinhopping
from auxillary import calc_rmse, load_pickle_file, calculate_theoretical_isotope_dist_from_sequence
from hx_rate_fit_functions import isotope_dist_from_PoiBin_calc_with_hx_rates, \
    calculate_intrinsic_exchange_rates_suggie
import Bio.PDB
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


class VariableStepSizeBH(object):
    """
    variable step size generator
    """

    def __init__(self, stepsize=0.05):
        """
        initialize the class
        :param stepsize: stepsize
        """
        self.stepsize = stepsize

    def __call__(self, x):
        s = self.stepsize
        x[0] += np.random.uniform(-2*s, 2*s)
        x[1:] += np.random.uniform(-s, s, x[1:].shape)
        return x


def save_pickle_file(data, outfile_name, dirpath):
    """
    save pickle file given the data (dictionary/class)
    :param data: data
    :param output_label: outfile path
    :return: None
    """
    with open(os.path.join(dirpath, outfile_name), 'wb') as outfile:
        pickle.dump(data, outfile, protocol=pickle.HIGHEST_PROTOCOL)


def plot_ms_data(msdatalist, output_dirpath, output_fname):
    """
    plot the ms data for in a pdf
    :param msdatalist: msdata list
    :param pdb_name: pdb name
    :return: none. Saves a pdf file
    """

    with PdfPages(os.path.join(output_dirpath, output_fname+'.pdf')) as pdf_plot:

        for ind, ms_data in enumerate(msdatalist):

            ms_data_norm = np.divide(ms_data, np.amax(ms_data))
            plt.plot(ms_data_norm, color='black')
            plt.title(str(ind))
            pdf_plot.savefig()
            plt.close()


def calc_back_exchange_without_free_energies(theo_isotope_dist, exp_isotope_dist, intrinsic_rates, init_guess=95):
    """
    calculate the back exchange. model back exchange according to the error distribution
    :param theo_isotope_dist:
    :param exp_isotope_dist:
    :param intrinsic_rates:
    :return:
    """
    opt = fmin_powell(lambda x: calc_rmse(isotope_dist_from_PoiBin_calc_with_hx_rates(isotope_dist=theo_isotope_dist,
                                                                                      timepoints=1e9,
                                                                                      rates=intrinsic_rates,
                                                                                      num_bins=len(exp_isotope_dist),
                                                                                      backexchange=expit(x)),
                                          exp_isotope_dist), x0=init_guess, disp=False)
    back_exchange = expit(opt)

    return back_exchange


###########
## this is the original one
##########
def poibin_isotope_dist_concat(timepoints, rates, num_bins, exp_isotope_dist_concat, backexchange_arr):
    """
    calculate the poibin isotope dist
    :param timepoints: timepoints
    :param rates: intrinsic rates
    :param num_bins: number of bins to include for isotope dist
    :param exp_isotope_dist_concat: experimental isotope dist concatenated
    :param backexchange: backexchange
    :return: poibin isotope dist
    """
    out_arr = np.zeros((len(timepoints), num_bins))
    for ind, timepoint in enumerate(timepoints):
        out_arr[ind, :] = isotope_dist_from_PoiBin_calc_with_hx_rates(exp_isotope_dist_concat,
                                                                      timepoint,
                                                                      rates,
                                                                      num_bins=num_bins,
                                                                      backexchange=backexchange_arr[ind])
    out_arr = np.ravel(out_arr)
    return out_arr


def hx_rate_fit_rmse(timepoints, rates, thr_isotope_dist_list, exp_isotope_dist_concat, num_bins, backexchange_arr):
    """

    :param timepoints: timepoints
    :param rates: rates
    :param thr_isotope_dist_list: theoretical isotope dist list
    :param num_bins: number of bins
    :param backexchange: backexchange
    :return: rmse between exp isotope dist concat and concat model from PoiBin Isotope dist
    """
    concat_model = poibin_isotope_dist_concat(timepoints, rates, num_bins, thr_isotope_dist_list, backexchange_arr)
    mean_sq_error = mean_squared_error(exp_isotope_dist_concat[exp_isotope_dist_concat > 0],
                                       concat_model[exp_isotope_dist_concat > 0])
    return mean_sq_error




def correct_back_exchange_variations(fitted_backexchange_val, number_of_timepoints, backexchange_corr_fpath):
    """
    correct the back exchange based on the variations
    :param fitted_backexchange:
    :param backexchange_corr_file:
    :return:
    """
    bx_corr = []
    with open(backexchange_corr_fpath, 'r') as corr_file:
        corr_data = corr_file.readlines()
        for ind, item in enumerate(corr_data[1:]):
            if ind < 5:
                bx_corr.append(0)
            else:
                bx_corr.append(float(item))


    bx_corr = np.array(([0] * (number_of_timepoints - len(bx_corr))) + bx_corr)

    print('heho')

    return bx_corr


##############
# this is the original one
##############
def fit_hx_rates_optimize(timepoints, thr_isotope_dist_list, exp_isotope_dist_list, num_rates, backexchange,
                          num_bins=None, n_iter=250, temp=0.00003, step_size=0.02, multi_fit_mode=False):
    """
    calculate the hx rates using the time points and the list of experimental isotope distribution
    :param timepoints: timepoints
    :param exp_isotope_dist_list: list of experimental isotope distributions
    :param backexchange: backexchange rate
    :return: fitted hx rates
    """

    backexchange_arr = np.array([backexchange for x in timepoints])
    if len(backexchange_arr) != len(timepoints):
        backexchange_arr = np.reshape(backexchange_arr, (len(timepoints),))

    exp_isotope_dist_concat = np.concatenate(exp_isotope_dist_list)

    init_rate_set = 0

    if multi_fit_mode:

        init_rates_list = [np.array([-2 for x in range(num_rates)]),
                           np.array([-5 for x in range(num_rates)]),
                           np.linspace(-7, 0, num_rates),
                           np.linspace(-7, -2, num_rates),
                           np.linspace(-8, -4, num_rates),
                           np.linspace(1, -12, num_rates)]

        prev_fun = 10

        for ind, init_rate in enumerate(init_rates_list):

            opt_ = basinhopping(lambda rates: hx_rate_fit_rmse(timepoints=timepoints,
                                                          rates=rates,
                                                          thr_isotope_dist_list=thr_isotope_dist_list,
                                                          exp_isotope_dist_concat=exp_isotope_dist_concat,
                                                          num_bins=num_bins,
                                                          backexchange_arr=backexchange_arr),
                           x0=init_rate,
                           niter=n_iter,
                           T=temp,
                           stepsize=step_size,
                           minimizer_kwargs={'options': {'maxiter': 1}})

            new_fun = opt_.fun

            if new_fun < prev_fun:
                opt = opt_
                init_rate_set = ind

            prev_fun = new_fun

    else:
        # init_rates = np.linspace(-8, -4, num_rates)
        init_rates = np.linspace(-4, -2, num_rates)
        opt = basinhopping(lambda rates: hx_rate_fit_rmse(timepoints=timepoints,
                                                          rates=rates,
                                                          thr_isotope_dist_list=thr_isotope_dist_list,
                                                          exp_isotope_dist_concat=exp_isotope_dist_concat,
                                                          num_bins=num_bins,
                                                          backexchange_arr=backexchange_arr),
                           x0=init_rates,
                           niter=n_iter,
                           T=temp,
                           stepsize=step_size,
                           minimizer_kwargs={'options': {'maxiter': 1}})

    return opt, init_rate_set


def plot_rates(intrinsic_rate, fitted_rate, output_path, pdb_name, output_add_label='01', extension='.png'):
    """
    plot intrinsic and fitted rate
    :param intrinsic_rate: sorted intrinsic rate list/array
    :param fitted_rate: sorted fitted rate list/array
    :param output_path: directory to save figure
    :param pdb_name: pdb name
    :param extension: file extension (png, pdf)
    :return: None. Saves the output file
    """
    plt.plot(intrinsic_rate, color='red', label='intrinsic_rate')
    plt.plot(fitted_rate, color='blue', label='fitted_rate')
    plt.ylabel('rate')
    plt.xlabel('res')
    plt.legend(loc='best', fontsize='small')
    plt.savefig(os.path.join(output_path, pdb_name+'_hx_rate_'+output_add_label+extension))
    plt.close()


def write_hx_rate_output(intrinsic_rates, fitted_rates, output_path, pdb_name, pD,
                         temp, backexchange, init_rate_set,
                         temp_bh, step_size_bh, n_iter_bh, fun_value, hx_rate_dict, output_add_label):
    """
    write the hx rate output to a csv file
    :param intrinsic_rate: sorted intrinsic rate list/array
    :param fitted_rate: sorted fitted rate list/array
    :param output_path: dirpath to save output
    :param pdb_name: pdb name
    :param pD: pD
    :param temp: temperature
    :param temp_bh: temp for basin hopping
    :param step_size_bh: step size for basin opping
    :param n_iter_bh: number of iterations for basin hopping
    :return: None. Saves the output to .csv file
    """
    param_main_header = 'PARAMS\n'
    param_header = 'pdb_name,pD,temp,backexchange,init_rate_set_ind,temp_bh,n_iter_bh,step_size_bh,fun_value_bh\n'
    param_data = '{},{},{},{},{},{},{},{},{}\n'.format(pdb_name, str(pD), str(temp), str(backexchange), str(init_rate_set), str(temp_bh),
                                                 str(n_iter_bh), str(step_size_bh), str(fun_value))

    # data section
    data_main_header = '\nOUTPUT\n'
    data_header = 'num,intrinsic_rates,fitted_rates\n'
    data_string = ''

    for ind, (intrin_rate, fitted_rate) in enumerate(zip(intrinsic_rates, fitted_rates)):
        line = '{},{},{}\n'.format(str(ind + 1), intrin_rate, fitted_rate)
        data_string += line

    output_string = param_main_header + param_header + param_data + data_main_header + data_header + data_string

    with open(os.path.join(output_path, pdb_name + '_hx_rate_' + output_add_label + '.csv'), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()

    ### write the pickle file
    outfile_name = pdb_name + '_hx_rate_' + output_add_label + '.pickle'
    save_pickle_file(hx_rate_dict, outfile_name, output_path)



def fit_hx_rates(pdb_path, timepoint_list, ms_data_list, output_path, num_bins=50, pD= 6.15, temp=298, nterm='',
                 cterm='', temp_bh=0.0003, step_size_bh=0.02, n_iter_bh=200, bx_corr=None, write_output=False,
                 plot_rate=False, output_add_label='01', multi_fit_rate_mode=False):
    """
    fit hx rates to individual residues, saves outputs, and returns the fitted rate
    :param pdb_path: path to pdb structure
    :param timepoint_list: timepoint list
    :param ms_data_list: ms data list
    :param output_path: dirpath to output files
    :param num_bins: number of bins to include in the isotope distribution
    :param pD: pD
    :param temp: temperature in Kelvin
    :param nterm: any addition to N-terminus of the structure
    :param cterm: any addition to C-terminus of the structure
    :param temp_bh: temperature set for basin hopping hx rate fitting optimization
    :param step_size_bh: step size set for basin hopping hx rate fitting optimization
    :param n_iter_bh: number of iterations for basin hopping hx rate fitting optimization
    :param bx_corr: correct backexchange with variations reported from the experiment
    :param write_output: write the hx rate to a .csv file
    :param plot_rate: boolean. If True, will output plot of intrinsic vs fitted rate
    :return: sorted fitted hx rate dictionary
    """

    output_dict = dict()

    # load the pdb structure using BioPython and generate a string of one letter residue sequence
    structure = Bio.PDB.PDBParser(QUIET=True).get_structure(pdb_path, pdb_path)
    residues = [res for res in structure[0].get_residues()]
    sequence_raw = ''.join([Bio.PDB.Polypeptide.three_to_one(aa.get_resname()) for aa in residues])

    output_dict['sequencce_raw'] = sequence_raw

    # add any additional residues at nterminal or cterminal ends. These variables are important later on while
    # calculating structural parameters such as burial depth, secondary structure elements, etc.

    sequence = nterm + sequence_raw + cterm
    len_sequence = len(sequence)

    output_dict['sequence'] = sequence

    # calculate the theoretical isotope distribution from the given sequence
    theoretical_isotope_dist = calculate_theoretical_isotope_dist_from_sequence(sequence, num_isotopes=num_bins)

    # calculate intrinsic rates based on the sequence, temperature, and pD
    intrinsic_rates = calculate_intrinsic_exchange_rates_suggie(sequence, Temperature=temp, pH=pD)
    intrinsic_rates[1] = 0.0  # can't properly calculate

    # normalize the ms data and make a list of isotope distribution
    isotope_dist_list = []
    for ind, ms_data in enumerate(ms_data_list):
        isotope_dist_list.append(ms_data/max(ms_data))

    # calculate back exchange rate using the last (or the fully deuterated**) isotopde distribution
    back_exchange = calc_back_exchange_without_free_energies(theo_isotope_dist=theoretical_isotope_dist,
                                                             exp_isotope_dist=isotope_dist_list[-1],
                                                             intrinsic_rates=intrinsic_rates,
                                                             init_guess=2)

    output_dict['backexchange'] = back_exchange

    # if bx_corr:
        # back_exchange_arr = correct_back_exchange_variations(fitted_backexchange=back_exchange,
        #                                                  number_of_timepoints=len(timepoint_list),
        #                                                  backexchange_corr_fpath=bx_corr)
        # output_dict['backexchange_corr_arr'] = back_exchange_arr




    # calculate how many rates to use for fitting (non-zero ones)
    len_rates = len([x for x in intrinsic_rates if x != 0])
    intrinsic_rates_nonzero_index = np.where(intrinsic_rates > 0)
    len_rates_ = len(intrinsic_rates_nonzero_index[0])

    # fitting rate using basin hopping optimization
    fit_rates, init_rate_set = fit_hx_rates_optimize(timepoints=timepoint_list,
                                      thr_isotope_dist_list=theoretical_isotope_dist,
                                      exp_isotope_dist_list=isotope_dist_list,
                                      num_rates=len_rates,
                                      backexchange=back_exchange,
                                      num_bins=num_bins,
                                      n_iter=n_iter_bh,
                                      temp=temp_bh,
                                      step_size=step_size_bh,
                                                     multi_fit_mode=multi_fit_rate_mode)

    # sort the fitted rates in an ascending manner
    fit_rate_all = [0, 0] + [x for x in fit_rates.x]
    sort_fitted_hx_rates_index = np.argsort(fit_rates.x)
    sort_fitted_hx_rates_res_index = intrinsic_rates_nonzero_index[0][sort_fitted_hx_rates_index]
    sorted_fitted_hx_rates = fit_rates.x[sort_fitted_hx_rates_index]


    fill_fit_rate_arr = np.zeros(len(intrinsic_rates) - len(sorted_fitted_hx_rates))
    sorted_fitted_hx_rates_filled = np.concatenate((sorted_fitted_hx_rates, fill_fit_rate_arr), axis=0)

    output_dict['intrinsic_rates'] = intrinsic_rates
    output_dict['sorted_fitted_hx_rates'] = sorted_fitted_hx_rates_filled

    output_dict['pD'] = pD
    output_dict['temp'] = temp
    output_dict['temp_bh'] = temp_bh
    output_dict['step_size_bh'] = step_size_bh
    output_dict['n_iter_bh'] = n_iter_bh
    output_dict['fit_rate_object'] = fit_rates


    # sort_intrinsic_rates_res_index = np.argsort(intrinsic_rates_nonzero_arr)
    # sorted_intrinsic_rates = intrinsic_rates_nonzero_arr[sort_intrinsic_rates_index]


    # writes the output to a csv file if True
    if write_output:
        pdb_path_components = os.path.split(pdb_path)
        pdb_name = pdb_path_components[1]

        write_hx_rate_output(intrinsic_rates, sorted_fitted_hx_rates_filled, output_path, pdb_name, pD, temp,
                             back_exchange, init_rate_set, temp_bh, step_size_bh, n_iter_bh, fit_rates.fun, output_dict,
                             output_add_label)



    # plots the figure if True
    if plot_rate:
        pdb_path_components = os.path.split(pdb_path)
        pdb_name = pdb_path_components[1]
        sorted_intrinsic_rates = sorted(np.log(intrinsic_rates[intrinsic_rates > 0]))
        plot_rates(sorted_intrinsic_rates, sorted_fitted_hx_rates, output_path, pdb_name, output_add_label)

    return sorted_fitted_hx_rates


if __name__ == '__main__':

    ## input all the following to run hx rate fitting

    dirpath = "/Users/smd4193/Documents/hx_ratefit_gabe"
    pickle_path = os.path.join(dirpath, "fp3_n_1900_HHH_rd4_0518.pdb_z6.0.pickle")
    pdb_path = os.path.join(dirpath, "HHH_rd4_0518.pdb")

    # if there is a bx correction file
    bx_corr_fpath = os.path.join(dirpath, "bx_corrections.txt")

    timepoints = [8, 9, 13, 20, 30, 60,
                  100, 180, 310, 480,
                  13 * 60, 19 * 60, 34 * 60, 57 * 60,
                  82 * 60, 158 * 60, 268 * 60, ((7 * 60) + 49) * 60,
                  ((13 * 60) + 24) * 60, ((18 * 60) + 24) * 60, ((26 * 60) + 50) * 60]  # in seconds

    pickle_obj = load_pickle_file(pickle_path)
    pickle_ms_data = pickle_obj[1]


    # todo: trim data may not be required. change this!
    trim_end_data = -1

    ms_data_list = []
    for ind in range(len(pickle_ms_data)+trim_end_data):
        ms_data_list.append(pickle_ms_data[ind]['comp_filtered'])

    # can plot ms data to view
    plot_ms_data(ms_data_list, dirpath, "HHH_rd4_0518")

    # MS data and time points need to be in the same order

    fitted_hx_rate_sorted = fit_hx_rates(pdb_path=pdb_path,
                                     timepoint_list=timepoints,
                                     ms_data_list=ms_data_list[1:],
                                     output_path=dirpath,
                                     num_bins=50,
                                     pD=6.15,
                                     temp=298,
                                     nterm='HM',
                                     temp_bh=0.00003,
                                     step_size_bh=0.02,
                                     n_iter_bh=20,
                                     bx_corr=None,
                                     write_output=True,
                                     plot_rate=True,
                                     output_add_label='test_',
                                         multi_fit_rate_mode=False)


    ###############################################
    ## used for testing things
    # dirpath = "/Users/smd4193/Documents/hx_ratefit_gabe"
    # pickle_path = os.path.join(dirpath, "fp3_n_1717_HHH_rd4_0997.pdb_z5.0.pickle")
    # pdb_path = os.path.join(dirpath, "HHH_rd4_0997.pdb")
    # pdb_patah_20053 = os.path.join(dirpath, "HHH_rd4_0518")
    # Nterm = 'HM'
    # Cterm = 'GSSGSSGNS'
    # structure = Bio.PDB.PDBParser(QUIET=True).get_structure(pdb_path, pdb_path)
    # residues = [res for res in structure[0].get_residues()]
    # sequence = ''.join([Bio.PDB.Polypeptide.three_to_one(aa.get_resname()) for aa in residues])
    # sequence = Nterm + sequence + Cterm
    #
    # pD = 6.15
    # temp = 295
    # intrinsic_rates = calculate_intrinsic_exchange_rates_suggie(sequence, Temperature=temp, pH=pD)
    # intrinsic_rates[1] = 0.0
    #
    # timepoints = [8, 9, 13, 20, 30, 60,
    #               100, 180, 310, 480,
    #               13 * 60, 19 * 60, 34 * 60, 57 * 60,
    #               82 * 60, 158 * 60, 268 * 60, ((7 * 60) + 49) * 60,
    #               ((13 * 60) + 24) * 60, ((18 * 60) + 24) * 60, ((26 * 60) + 50) * 60]  # in seconds
    #
    # pickle_obj = load_pickle_file(pickle_path)
    #
    # datafile = dict()
    #
    # datafile['output'] = pickle_obj[1]
    # isotope_dist_list = []
    # for ind in range(len(datafile['output'])):
    #     datafile['output'][ind]['major_species_integrated_intensities'] = datafile['output'][ind]['comp_filtered']
    #     isotope_dist_list.append(datafile['output'][ind]['major_species_integrated_intensities'] / max(
    #         datafile['output'][ind]['major_species_integrated_intensities']))
    #
    # exp_isotope_dist_final_timepoint = datafile['output'][-1]['major_species_integrated_intensities']/max(datafile['output'][-1]['major_species_integrated_intensities'])
    #
    # theo_isotope = calculate_theoretical_isotope_dist_from_sequence(sequence)
    # # back_exch = 0.88
    # back_exch = calc_back_exchange_without_free_energies(theo_isotope,
    #                                                      exp_isotope_dist_final_timepoint,
    #                                                      intrinsic_rates=intrinsic_rates,
    #                                                      init_guess=2)
    #
    # len_rates = len([x for x in intrinsic_rates if x!=0])
    #
    # fit_rates = fit_hx_rates_optimize(timepoints, theo_isotope, isotope_dist_list[1:],
    #                          num_rates=len_rates,
    #                          backexchange=back_exch,
    #                          n_iter=5,
    #                          num_bins=50)
    #
    # print('backexch ', back_exch)
    # print(fit_rates.x)
    # plt.plot(sorted(fit_rates.x), color='blue')
    # plt.plot(sorted(np.log(intrinsic_rates[intrinsic_rates > 0])), color='red')
    # plt.show()
    # plt.close()
    # print('gheho')
    #
    # print('heho')