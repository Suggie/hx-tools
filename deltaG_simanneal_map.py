"""
use simulated annelaing to map delta G value based on HX rate fitting and protein structure
"""
from __future__ import print_function
import sys
sys.path.insert(1, '/Users/smd4193/PycharmProjects/hx-tools')
import os
import optparse
import copy
import Bio.PDB
from Bio.PDB import *
from Bio.PDB.DSSP import dssp_dict_from_pdb_file
from Bio.SeqUtils.ProtParam import ProteinAnalysis
import numpy as np
import pandas as pd
import pickle
import random
from coupled_simanneal import CoupledAnnealer
import simanneal_2
from simanneal_2 import Annealer
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from memory_profile import profile

def create_aa_dict():
    """
    Create a dictionary containing amino acid three letter, one letter, polar, non polar residue classes
    :return: dictionary
    """
    aa_dict = dict()
    aa_dict['aa_code'] = dict()
    aa_dict['aa_code']['three_letter'] = ['Arg', 'His', 'Lys', 'Asp', 'Glu', 'Ser', 'Thr', 'Asp', 'Gln', 'Cys', 'Gly',
                                          'Pro', 'Ala', 'Ile', 'Leu', 'Met', 'Phe', 'Trp', 'Tyr', 'Val']
    aa_dict['aa_code']['one_letter'] = ['R', 'H', 'K', 'D', 'E', 'S', 'T', 'N', 'Q', 'C', 'G', 'P', 'A', 'I', 'L', 'M',
                                        'F', 'W', 'Y', 'V']
    # polar amino acids contain polar uncharged and charged side chains. Only in one letter code
    aa_dict['polar_amino_acids'] = ['D', 'E', 'G', 'H', 'K', 'N', 'P', 'Q', 'R', 'S', 'T']
    aa_dict['non_polar_amino_acids'] = ['A', 'C', 'F', 'I', 'L', 'M', 'V', 'W', 'Y']
    return aa_dict


def smooth_dist_cutoff(x):
    """
    exponential function for smooth continuous function
    :param x: variable
    :return: function output
    """
    return 1. / (1. + (np.exp(x - 10)))


def load_hx_rate_file(hx_rate_fpath):
    """
    load the hx rate file and gives intrinsic and fitted rates
    :param hx_rate_fpath: hx rate file path
    :return: intrinsic and fitted rates list
    """
    df = pd.read_csv(hx_rate_fpath, skiprows=5)
    hx_rate_dict = dict()
    hx_rate_dict["intrinsic_rates"] = df['intrinsic_rates'].values
    # hx_rate_dict["intrinsic_rate_res_index"] = df['intrinsic_rate_res_index'].values
    hx_rate_dict["fitted_rates"] = df['fitted_rates'].values
    # hx_rate_dict["fitted_rate_res_index"] = df['fitted_rate_res_index'].values
    return hx_rate_dict


def distance_to_nonpolar_residues(sequence):
    """
    Mapping of each residue's nearest distance to a non polar residue. 0 is 1 residue away, -1 is 2 residues away.
    1 indicates that it itself is a non polar residue. positive distances mean distance from nearest polar residues.
    It searches the nearest non polar residues using a window search. Creates a window by iteratively adding an amino
    acid on the either side (n and c terminal ends). When a non polar residue is included in the window, the algorithm
    detects it and finds how many index far it is. That index is then stored in the output array.
    :param sequence: string of input sequence
    :return: array of distances [0,-1,-2,-3,0,1,2,1,0] etc as described above
    """
    aa_dict = create_aa_dict()
    dist_arr = []
    for index in range(len(sequence)):
        current_residue = sequence[index]
        # if the current residue is a polar type, then start generating windows to search for the nearest non polar
        # amino acid and find the distance in terms of index distance
        if current_residue in aa_dict['polar_amino_acids']:
            for num in range(1, len(sequence)):
                window_bounds = [max([0, index - num]), min([len(sequence), index+num+1])]
                window = sequence[window_bounds[0]:window_bounds[1]]
                for non_polar_res in aa_dict['non_polar_amino_acids']:
                    if non_polar_res in window:
                        dist_arr.append(- num + 1)
                        break
                if non_polar_res in window:
                    break
            if num == len(sequence) - 1:
                dist_arr.append(- num + 1)

        else:
            for num in range(1, len(sequence)):
                window_bounds = [max([0, index - num]), min([len(sequence), index+num+1])]
                window = sequence[window_bounds[0]:window_bounds[1]]
                for polar_res in aa_dict['polar_amino_acids']:
                    if polar_res in window:
                        dist_arr.append(num)
                        break
                if polar_res in window:
                    break
            if num == len(sequence) - 1:
                dist_arr.append(num)
    dist_arr = np.array(dist_arr)
    return dist_arr


def distance_to_secondary_structure(secondary_structure_string):
    """
    Mapping of loop to the nearest helix/strand in -ve index distance and helix/strand to the nearest loop in +ve index
    distance
    :param secondary_structure_string: string of secondary structure assignments that contain L, H, and E
    :return: distance_array [0,-1,-2,-3,0,1,2,1,0] etc as described above
    """
    dist_arr = []
    for index in range(len(secondary_structure_string)):
        current_sec_struct = secondary_structure_string[index]
        # if current secondary structure is a loop 'L'
        if current_sec_struct == 'L':
            for num in range(1, len(secondary_structure_string)):
                window_bounds = [max([0, index - num]), min([len(secondary_structure_string), index + num + 1])]
                window = secondary_structure_string[window_bounds[0]:window_bounds[1]]
                # if helix 'H' or beta strand 'E' in the window, then append the distance to the dist_arr
                if 'E' in window or 'H' in window:
                    dist_arr.append(- num + 1)
                    break
            if num == len(secondary_structure_string) - 1:
                dist_arr.append(- num + 1)

        # if current secondary structure is either 'H' or beta strand 'E'
        else:
            for num in range(1, len(secondary_structure_string)):
                window_bounds = [max([0, index - num]), min([len(secondary_structure_string), index + num + 1])]
                window = secondary_structure_string[window_bounds[0]:window_bounds[1]]
                if 'L' in window:
                    dist_arr.append(num)
                    break
            if num == len(secondary_structure_string) - 1:
                dist_arr.append(num)

    dist_arr = np.array(dist_arr)
    return dist_arr


def get_dssp_sec_structure_assignment(pdb_filepath):
    """
    assigns secondary structure to the given structure object from biopython. Uses the biopython dssp module. It
    requires you to have working dssp executable
    # conda install -c salilab dssp
    Biopython will automatically find dssp and use it. In this case, you don't have to specify the dssp executable path
    # change loop assignments (S, C, T, -) to one letter assignment 'L'
    :param pdb_filepath: filepath for the pdb file
    :param dssp_exec_path: dssp executable path
    :return: dssp assignment. Dssp dict contains the following list for each dict item.
            aa, -> aa one letter code
            ss, -> secondary structure assignment
            acc, -> number of H2O molecules in contact with this residue * 10 or residue water exposed surface in Angstrom **2
            phi, -> backbone torsion angle
            psi, -> backbone torsion angle
            dssp_index, -> dssp index starting with 1
            NH_O_1_relidx, -> Index of O residue in which it forms H bond with NH group
            NH_O_1_energy, -> H bond energy of the previous scenario
            O_NH_1_relidx, -> Index of NH residue in which it forms H bond with OH group
            O_NH_1_energy, -> H bond energy of the previous scenario
            NH_O_2_relidx, -> Index of O residue in which it forms H bond with the NH group
            NH_O_2_energy, -> H bond energy of the previous scenario
            O_NH_2_relidx, -> Index of NH group in which it forms H bond with the OH group
            O_NH_2_energy -> H bond energy of the previous scenario
    """
    dssp_output = dssp_dict_from_pdb_file(pdb_filepath)
    sec_struct_string = ''
    for ind, key_values in enumerate(dssp_output[0].values()):
        sec_struct = key_values[1]
        if sec_struct == 'S' or sec_struct == 'T' or sec_struct == 'C' or sec_struct == '-':
            sec_struct = 'L'
        sec_struct_string += sec_struct
    return sec_struct_string


class HBond(object):
    """
    a class object to get hbond information from a pdb structure
    """

    def __init__(self, pdb_fpath, nterm='', cterm='', hbond_length=2.7, hbond_angle=120):
        """
        initialize the class with the file path for the pdb structure and nterm/cterm additions
        :param pdb_fpath: pdb file path
        """
        self.pdb_fpath = pdb_fpath
        self.nterm = nterm
        self.cterm = cterm
        self.hbond_length = hbond_length
        self.hbond_angle = hbond_angle

        # todo: take these out later
        # todo: take self objects out later if not necessary for the hbond output
        structure = Bio.PDB.PDBParser(QUIET=True).get_structure(self.pdb_fpath, self.pdb_fpath)
        model = structure[0]

        # outputs from this class
        self.hbond_dict = self.get_hbond_stat_dict(model)
        self.pdb_model = model

    def hbond_depth(self, model, hbond_list, all_hbond_list):
        """
        calculates the hbond depth as distance of nearest hbond from each residue
        :param model: model object
        :param all_hbond_list: all hbond list from get_hbond_stat_dict
        :return: hbond_depth_array, updated all_hbond_list
        """
        residues = [x for x in model.get_residues()]
        hbond_depth_list = [1 if x in all_hbond_list else 0 for x in range(len(residues))]
        for num in range(1, len(residues) - 1):
            if hbond_depth_list[num - 1: num + 2] == [1, 0, 1]:
                hbond_depth_list[num] = 1
                all_hbond_list.append(num)

        num2 = 1
        while hbond_depth_list.count(num2) > 1:
            for index in range(num2, len(residues) - num2):
                if hbond_depth_list[index - 1: index + 2] in [[num2, num2, num2], [num2 + 1, num2, num2]]:
                    hbond_depth_list[index] += 1
            num2 += 1

        hbond_depth_arr = np.array(hbond_depth_list) * np.array(
            [1 if x in hbond_list else 0 for x in range(len(residues))])

        # include the nterm and cterm additional residues
        hbond_depth_arr = np.array([0 for x in self.nterm] + list(hbond_depth_arr) + [0 for x in self.cterm])

        return hbond_depth_arr, all_hbond_list

    def get_hbond_stat_dict(self, model):
        """
        get hbond list, hbond dist, hbond pairs,
        :return: dict of lists
        """

        hbond_stat_dict = dict()

        residues = [x for x in model.get_residues()]

        hbond_list = []
        hbond_dists = [None for x in residues]
        hbond_angles = [None for x in residues]
        hbond_partners = [None for x in residues]
        all_hbonds = []
        sc_hbonds = []
        pairlist = []
        atoms_in_radius = [0 for x in self.nterm]
        for index, res in enumerate(residues):
            atoms_in_radius.append(
                sum([smooth_dist_cutoff(np.linalg.norm(res['N'].coord.astype(float) - atm.coord.astype(float)))
                     for atm in model.get_atoms() if 'H' not in atm.get_name()]))
            if 'H' in res:
                for ind, res2 in enumerate(residues):
                    if ind == index:
                        continue
                    new_hbond_dist = np.linalg.norm(res['H'].coord.astype(float) - res2['O'].coord.astype(float))
                    new_hbond_angle = np.rad2deg(min([Bio.PDB.calc_angle(res['N'].get_vector(),
                                                                         res['H'].get_vector(),
                                                                         res2['O'].get_vector()),
                                                      Bio.PDB.calc_angle(res['H'].get_vector(),
                                                                         res2['O'].get_vector(),
                                                                         res2['C'].get_vector())]))
                    if (new_hbond_dist < self.hbond_length) and (new_hbond_angle > self.hbond_angle):
                        hbond_list.append(index)
                        all_hbonds.append(index)
                        all_hbonds.append(ind)
                        hbond_partners[index] = ind + len(self.nterm)
                        hbond_dists[index] = new_hbond_dist
                        hbond_angles[index] = new_hbond_angle

                    for o_atom in [x for x in res2 if x.name[0] == 'O' and x.name != 'O']:
                        new_hbond_dist = np.linalg.norm(res['H'].coord.astype(float) - o_atom.coord.astype(float))
                        if new_hbond_dist < self.hbond_length:
                            sc_hbonds.append(index)

                    if index < ind:
                        if 'H' in res and 'H' in res2:
                            h_distance = np.linalg.norm(res['H'].coord.astype(float) - res2['H'].coord.astype(float))
                            if h_distance < 13:
                                pairlist.append((index + len(self.nterm), ind + len(self.nterm), h_distance))

        atoms_in_radius += [0 for x in self.cterm]

        hbond_list = sorted(set(hbond_list))
        all_hbonds = sorted(set(all_hbonds))

        hbond_dists = np.array([None for x in self.nterm] + list(hbond_dists) + [None for x in self.cterm])
        hbond_angles = np.array([None for x in self.nterm] + list(hbond_angles) + [None for x in self.cterm])
        hbond_partners = np.array([None for x in self.nterm] + list(hbond_partners) + [None for x in self.cterm])

        hbond_depth_array, all_hbond_list = self.hbond_depth(model, hbond_list, all_hbonds)

        hbond_bool_list = np.array([False if x == 0 else True for x in hbond_depth_array])

        hbond_bool_num_list = []
        for item in hbond_bool_list:
            if item:
                hbond_bool_num_list.append(1)
            else:
                hbond_bool_num_list.append(0)
        hbond_bool_num_list = np.array(hbond_bool_num_list)

        hbond_stat_dict['hbond_list'] = hbond_list
        hbond_stat_dict['all_hbond_list'] = all_hbond_list
        hbond_stat_dict['hbond_depth_array'] = hbond_depth_array
        hbond_stat_dict['hbond_bool_list'] = hbond_bool_list
        hbond_stat_dict['hbond_bool_num_list'] = hbond_bool_num_list
        hbond_stat_dict['burial'] = np.array(atoms_in_radius)
        hbond_stat_dict['pairlist'] = pairlist
        hbond_stat_dict['hbond_dist_list'] = hbond_dists
        hbond_stat_dict['hbond_angle_list'] = hbond_angles
        hbond_stat_dict['hbond_partner_list'] = hbond_partners

        return hbond_stat_dict


def calc_free_energy_from_hx_rates(hx_instrinsic_rate, hx_meas_rate, temperature=295, r_constant=1.987204e-3):
    """
    calculate free energy of opening (change in protein energy landscape upon opening or unfolding)
    :param hx_instrinsic_rate: hx intrinsic rate
    :param hx_meas_rate: hx measured rate
    :param r_constant: 1.987204e-3 kcal mol-1 K-1
    :param temperature: temperature in Kelvin
    :return: free energy of opening = - R T ln (k_op)
    """
    k_op = hx_meas_rate/(hx_instrinsic_rate - hx_meas_rate)
    free_energy = -r_constant * temperature * np.log(k_op)
    return free_energy


def calc_net_charge_at_pH(protein_sequence, pH=6.15):
    """
    calculate net charge at certain pH
    :param protein_sequence:
    :param pH:
    :return:
    """
    analyses_seq = ProteinAnalysis(protein_sequence)
    net_charge = analyses_seq.charge_at_pH(pH)
    return net_charge


def calc_hx_rate_from_free_energy(free_energy, hx_intrinsic_rate, temperature=298, r_constant=1.987204e-3):
    """
    calculate the hx rate given the opening free energy, intrinsic rate, temperature, and r constant
    :param free_energy: free energy in kcal mol-1
    :param hx_intrinsic_rate: hx intrinsic rate
    :param temperature: tempreature in kelvin
    :param r_constant: r constant
    :return: hx rate
    """
    k_op = np.exp(-free_energy/(r_constant*temperature))
    hx_rate = (hx_intrinsic_rate * k_op)/(1 + k_op)
    return hx_rate


def assemble_hx_rate_from_free_energy_v2(energy_file, hx_rate_dict, nterm, cterm):
    """
    calculate hx rate from the energy file and assemble
    :param energy_file:
    :param hx_rate_dict:
    :return:
    """
    fes = []
    with open(energy_file, 'r') as file:
        energy_lines = file.read().splitlines()
        fes = [float(x.split()[-1]) for x in energy_lines[1:]]
        fes[0] = 0
        fes[1] = 0

    fes = [0]*len(nterm) + fes + [0]*len(cterm)
    fes = np.array(fes)
    rate_from_fes = []
    for res_index, free_energy in enumerate(fes):
        try:
            intrinsic_rate = hx_rate_dict['intrinsic_rates'][np.where(hx_rate_dict['intrinsic_rate_res_index'] == res_index)][0]
            hx_rate = calc_hx_rate_from_free_energy(free_energy, intrinsic_rate)
            rate_from_fes.append(hx_rate)
        except:
            pass
    rate_from_fes = np.log(np.array(sorted(rate_from_fes)))
    return rate_from_fes


def corr_fe_with_net_charge(fe, net_charge):
    fe_corr = fe - (net_charge * -0.09203943)
    return fe_corr


def gen_free_energy_grid_from_hx_rates(active_hx_rates, active_intrinsic_hx_rate, min_free_energy=0,
                                       temp=295, r_constant=1.9872036e-3, net_charge_corr=False, net_charge=None):
    """
    generate free energy grid as N x M were there are N hx rate list and M intrisnsic rate list
    :param active_hx_rates: hx fitted rate (measured) for residues that are allowed to exchange
    :param active_intrinsic_hx_rate: intrinsic hx rate based on sequence for residues that are allowed to exchange
    :return: free energy matrix
    """
    free_energy_arr = np.zeros((len(active_hx_rates), len(active_intrinsic_hx_rate)))

    for ind1, intrinsic_hx_rate in enumerate(active_intrinsic_hx_rate):
        for ind2, fit_hx_rate in enumerate(active_hx_rates):
            if fit_hx_rate >= intrinsic_hx_rate:
                free_energy_arr[ind1, ind2] = min_free_energy
            else:
                fe = calc_free_energy_from_hx_rates(intrinsic_hx_rate, fit_hx_rate, temp,
                                                                         r_constant)
                if net_charge_corr:
                    fe_corr = corr_fe_with_net_charge(fe, net_charge)
                    free_energy_arr[ind1, ind2] = fe_corr
                else:
                    free_energy_arr[ind1, ind2] = fe
                # free_energy_arr[ind1, ind2] = calc_free_energy_from_hx_rates(intrinsic_hx_rate, fit_hx_rate, temp,
                #                                                          r_constant)

    return free_energy_arr


def prep_sa_input(deltag_interpol_fpath, hx_rate_fpath, pdb_fpath, output_dirpath, outfile_label='deltaG',
                  hx_rate_from_free_energy=False, comp_data_fpath=None, nterm='', cterm='', hbond_length=2.7,
                  hbond_angle=120, min_free_energy=-10, pH=6.15, temp=295, net_charge_corr=False,
                  r_constant=1.9872036e-3, min_comp_free_energy=0.5, sa_energy_weights=None):
    """
    prepare the input for simulated annealing method
    :param deltag_interpol_fpath: deltag interpolation optimization pickle
    file (pre-computed from a set of standard proteins)
    :param hx_rate_fpath: hx rate file path
    :param pdb_fpath: pdb file path
    :param comp_data_fpath: comparison data file path. CSV File with residue num in 1st column and delta G values in
                            2nd column. with header = 1
    :param nterm: any nterm additions
    :param cterm: any cterm additions
    :param hbond_length: hydrogen bond length
    :param hbond_angle: hydrogen bond angle
    :return: dictionary of items necessary for SA module
    """
    # load the delta g interpolation pickle file
    with open(deltag_interpol_fpath, 'rb') as pk_file:
        deltag_interpol = pickle.load(pk_file, encoding='latin1')

    # load the intrinsic rates from the file
    # todo: need to change how hx fit rate and int rate are being saved! Look at get_info_from_gabe_pickle_files for info
    hx_rate_dict = load_hx_rate_file(hx_rate_fpath)
    hx_intrinsic_rate = hx_rate_dict["intrinsic_rates"]
    hx_fit_rate = hx_rate_dict["fitted_rates"]
    # hx_intrinsic_rate, hx_fit_rate = load_hx_rate_file(hx_rate_fpath)

    hbond_class = HBond(pdb_fpath, nterm, cterm, hbond_length, hbond_angle)
    # hbond_dict = hbond_class.hbond_dict
    model = hbond_class.pdb_model

    # len_slow_hx_res = len(hx_intrinsic_rate[hx_fit_rate == 0])
    # len_fast_hx_res = len([x for x in nterm if x != 'P']) + len([x for x in cterm if x != 'P'])

    residues = [x for x in model.get_residues()]
    sequence_raw = ''.join([Polypeptide.three_to_one(aa.get_resname()) for aa in residues])

    sequence = nterm + sequence_raw + cterm

    seq_len = len(sequence)
    rawseq_len = len(sequence_raw)

    raw_seq_num = np.zeros(seq_len)
    for num in range(seq_len):
        if num > len(nterm) - 1:
            if num < seq_len - len(cterm):
                raw_seq_num[num] = num - len(nterm) + 1

    # create a distance to non polar res  array
    dist_to_nonpolar_res = distance_to_nonpolar_residues(sequence)

    # create a distance to sec struct array
    sec_struct_string = get_dssp_sec_structure_assignment(pdb_fpath)
    sec_struct_string_2 = 'L' * len(nterm) + sec_struct_string + 'L' * len(cterm)

    # sec_struct_string_3 = 'LLLHHHHHHHHHHHHHLLLHHHHHHHHHHHLLLHHHHHHHHHHHL'

    dist_to_sec_struct = distance_to_secondary_structure(sec_struct_string_2)


    hx_allowed_seq_bool_index = [False for x in nterm] + [False if x == 'P' else True for x in sequence_raw] + [False
                                                                                                                for x in
                                                                                                                cterm]

    hx_allowed_seq_bool_index[0:2] = [False, False]

    hx_allowed_seq_bool_index = np.array(hx_allowed_seq_bool_index)

    len_hx_allowed = sum(hx_allowed_seq_bool_index)

    sequence_raw_list = [str(x) for x in sequence_raw]
    proline_res_ind = []
    for ind, aa in enumerate(sequence_raw_list):
        if aa == 'P':
            proline_res_ind.append(ind)

    hx_allowed_index = []
    hx_allowed_raw_seq_bool_index = [False if x == 'P' else True for x in sequence_raw]
    hx_allow_index_test = np.where(hx_allowed_seq_bool_index == True)[0]
    ## the first two residues are always 0 so hx not allowed in int rate. So start with index position 2
    for ind, hx_allow_bool in enumerate(hx_allowed_seq_bool_index):
        if hx_allow_bool:
            hx_allowed_index.append(ind)
    hx_allowed_index = np.array(hx_allowed_index)
    ## hx allowed index is the index allowed for hx in the sequence

    # active_hx_rates = np.exp(hx_fit_rate[0:len_hx_allowed]) # see what it does
    if hx_rate_from_free_energy:
        hx_rate_fes = assemble_hx_rate_from_free_energy_v2(comp_data_fpath, hx_rate_dict, nterm, cterm)
        active_hx_rates = np.exp(hx_rate_fes[0:len_hx_allowed])
        active_hx_rates_noexp = hx_rate_fes[0:len_hx_allowed]
    else:
        active_hx_rates_0 = sorted(hx_fit_rate[0:len_hx_allowed])
        active_hx_rates = np.exp(sorted(active_hx_rates_0))
        # active_hx_rates = np.array(sorted(np.exp(hx_fit_rate[0:len_hx_allowed])))
        active_hx_rates_noexp = hx_fit_rate[0:len_hx_allowed]

    # the first two hx intrinsic rates are always 0 so those are excluded from the intrinsic rate list. So when
    # creating an active intrinsic rate list, use the bool index starting from position 2 index.

    # len_slow_rates_ind = len(hx_intrinsic_rate[hx_intrinsic_rate == 0])
    #
    # len_fast_rates_ind = len([x for x in nterm if x != 'P']) + len([x for x in cterm if x != 'P'])
    # if len(nterm) > 0:
    #     if nterm[0] != 'P':
    #         len_fast_rates_ind = -1


    # active_intrinsic_hx_rate = np.exp(hx_intrinsic_rate[hx_rate_allowed_index])
    # active_int_hx_rate = np.exp(hx_intrinsic_rate[len_slow_rates_ind : (len(hx_intrinsic_rate) if len_fast_rates_ind == 0 else len_fast_rates_ind)])
    # active_intrinsic_hx_rate = np.exp(hx_intrinsic_rate)
    # active_intrinsic_hx_rate = np.exp(hx_intrinsic_rate)
    # active_intrinsic_hx_rate = np.exp(hx_intrinsic_rate[0:len_hx_allowed])
    active_hx_intrinsic_rate = hx_intrinsic_rate[hx_allowed_seq_bool_index]
    # active_intrinsic_hx_rate_noexp = hx_intrinsic_rate[0:len_hx_allowed]
    # print(active_intrinsic_hx_rate)
    # print(active_hx_rates)

    # create free energy grid
    net_charge = calc_net_charge_at_pH(sequence, pH=pH)
    free_energy_grid = gen_free_energy_grid_from_hx_rates(active_hx_rates, active_hx_intrinsic_rate,
                                                          min_free_energy=min_free_energy, temp=temp,
                                                          r_constant=r_constant, net_charge_corr=net_charge_corr,
                                                          net_charge=net_charge)
    # free_energy_grid_w_netcharge = gen_free_energy_grid_from_hx_rates(active_hx_rates, active_hx_intrinsic_rate,
    #                                                       min_free_energy=min_free_energy, temp=temp,
    #                                                       r_constant=r_constant, net_charge_corr=True,
    #                                                       net_charge=net_charge)
    # print('heho')

    h_h_pairlist = []
    # hbond_bool_list = np.array([0.0 if x == 0 else 1 for x in hbond_class.hbond_dict['hbond_depth_array']])
    for pairlist in hbond_class.hbond_dict['pairlist']:
        aa1, aa2, h_h_dist = pairlist[0], pairlist[1], pairlist[2]
        if hbond_class.hbond_dict['hbond_bool_list'][aa1] and hbond_class.hbond_dict['hbond_bool_list'][aa2]:
            h_h_pairlist.append([aa1, aa2, h_h_dist])
    h_h_pairlist = np.array(h_h_pairlist)


    hx_active_index = np.array([sum(hx_allowed_seq_bool_index[0:x]) for x in range(len(hx_allowed_seq_bool_index))],
                               dtype=object)
    for num in range(len(hx_active_index)-1):
        if hx_active_index[num+1] == hx_active_index[num]:
            hx_active_index[num] = None

    active_aa1, active_aa2 = [], []
    for aa1, aa2 in zip(h_h_pairlist[:, 0], h_h_pairlist[:, 1]):
        active_aa1.append(hx_active_index[int(aa1)])
        active_aa2.append(hx_active_index[int(aa2)])

    active_aa1, active_aa2 = np.array(active_aa1), np.array(active_aa2)

    pair_energies_multi_grid = np.zeros((len_hx_allowed, len_hx_allowed, len_hx_allowed, len_hx_allowed))

    for ind, (aa1, aa2, hh_dist) in enumerate(zip(active_aa1, active_aa2, h_h_pairlist[:, 2])):
        for num1 in range(len_hx_allowed):
            for num2 in range(len_hx_allowed):
                fe1 = free_energy_grid[aa1, num1]
                fe2 = free_energy_grid[aa2, num2]
                fe_dist = abs(fe1 - fe2)
                interp = deltag_interpol(hh_dist, fe_dist)
                pair_e = -np.log(interp)
                pair_energies_multi_grid[aa1, aa2, num1, num2] = pair_e

    states = list(range(sum(hx_allowed_seq_bool_index)))

    #hbonds_rank_best and hbonds_rank_worst
    num_hbonds = sum(hbond_class.hbond_dict['hbond_bool_num_list'])
    num_no_hbonds = len(states) - num_hbonds
    hbond_rank_best_ranks = np.arange(num_no_hbonds, len(states))
    hbond_rank_best = sum(hbond_rank_best_ranks)
    hbond_rank_worst = sum(np.arange(num_hbonds))

    print('heho')


    sa_input = dict()
    sa_input['output_dir'] = output_dirpath
    sa_input['pdb_fname'] = os.path.split(pdb_fpath)[1]
    sa_input['outfile_label'] = outfile_label
    sa_input['sa_state'] = states
    sa_input['prot_raw_sequence'] = sequence_raw
    sa_input['prot_sequence'] = sequence
    sa_input['raw_seq_num_array'] = raw_seq_num
    sa_input['nterm'] = nterm
    sa_input['cterm'] = cterm
    sa_input['net_charge'] = net_charge
    sa_input['hbond_class'] = hbond_class
    sa_input['active_hx_rates'] = active_hx_rates
    sa_input['free_energy_grid'] = free_energy_grid
    sa_input['active_res_set_1'] = active_aa1
    sa_input['active_res_set_2'] = active_aa2
    sa_input['len_hx_allowed'] = len_hx_allowed
    sa_input['hx_allowed_index'] = np.array(hx_allowed_index)
    sa_input['hx_allowed_bool_seq'] = hx_allowed_seq_bool_index
    sa_input['pair_energies_multi_grid'] = pair_energies_multi_grid
    sa_input['distance_to_nonpolar_res'] = dist_to_nonpolar_res
    sa_input['distance_to_sec_struct'] = dist_to_sec_struct
    sa_input['hbond_rank_best'] = hbond_rank_best
    sa_input['hbond_rank_worst'] = hbond_rank_worst
    sa_input['min_comp_free_energy'] = min_comp_free_energy

    if sa_energy_weights:
        sa_input['weights'] = sa_energy_weights  # way to manually put weights for sim anneal
    else:
        sa_input['weights'] = dict()
        sa_input['weights']['pair_energy'] = 50
        sa_input['weights']["full_burial"] = 120
        sa_input['weights']["hbond_burial"] = 14
        sa_input['weights']["hbond_rank"] = 60
        sa_input['weights']["distance_to_sec_struct"] = 60  # used to be zero
        sa_input['weights']["distance_to_nonpolar_res"] = 45  # used to be zero
        sa_input['weights']["top_stdev"] = 10
        sa_input['weights']["comp_deltag_rmse"] = 1

    if comp_data_fpath:
        df = pd.read_csv(comp_data_fpath)
        resnum, deltag_val = df.iloc[:, 0].values, df.iloc[:, 1].values
        res_index = np.subtract(resnum, 1)
        if len(proline_res_ind) == 0:
            res_index_final = res_index
            deltag_val_final = deltag_val
        else:
            res_index_final = []
            deltag_val_final = []
            for ind, (res_ind, deltag_value) in enumerate(zip(res_index, deltag_val)):
                if res_ind not in proline_res_ind:
                    res_index_final.append(res_ind)
                    deltag_val_final.append(deltag_value)
            res_index_final = np.array(res_index_final)
            deltag_val_final = np.array(deltag_val_final)
        deltag_comp = np.zeros(len(sequence_raw))
        deltag_comp[res_index_final] = deltag_val_final
        deltag_comp_allowed = deltag_comp[hx_allowed_raw_seq_bool_index]
        deltag_comp_allowed[deltag_comp_allowed == 0] = min_comp_free_energy

        sa_input['comp_data'] = dict()
        sa_input['comp_data']['deltaG'] = deltag_comp_allowed
    else:
        sa_input['comp_data'] = None

    return sa_input


def test_pair_energy(sa_input_dict):
    state = sa_input_dict['sa_state']
    pair_e = []
    pair_e_add = 0
    for aa1, aa2 in zip(sa_input_dict['active_res_set_1'], sa_input_dict['active_res_set_2']):
        eng = sa_input_dict['pair_energies_multi_grid'][aa1, aa2, state[aa1], state[aa2]]
        pair_e_add += eng
        pair_e.append(eng)
    avg_pair_e = np.average(pair_e)
    return avg_pair_e


def test_full_burial_corr(sa_input_dict):
    """
    calculate the correlation coeffs between an array of free energy grid and burial distribution. Return the Cij
    :return:
    """
    state = sa_input_dict['sa_state']
    current_free_energy = np.array(
        [sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state)])
    corr = 0
    corr2 = 0
    if min(current_free_energy) == max(current_free_energy):
        corr += 0
    else:
        max_current_free_energy = np.maximum(current_free_energy, 0)
        max_burial = np.maximum(60, sa_input_dict['hbond_class'].hbond_dict['burial'][
            sa_input_dict['hx_allowed_index']])
        corr_coef2 = np.corrcoef(max_current_free_energy, max_burial)
        corr_coef = np.corrcoef(current_free_energy, sa_input_dict['hbond_class'].hbond_dict['burial'][
            sa_input_dict['hx_allowed_index']])
        corr += -corr_coef[0][1]
        corr2 += -corr_coef2[0][1]
        print('heho')
    return corr


def test_distance_to_sec_struct_energy(sa_input_dict):
    """
    calculate the correlation coeffs between an array of free energy grid and burial distribution. Return the Cij
    :return:
    """
    state = sa_input_dict['sa_state']
    current_free_energy = np.array(
        [sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state)])
    corr = 0
    corr2 = 0
    if min(current_free_energy) == max(current_free_energy):
        corr += 0
    else:

        corr_coef2 = np.corrcoef(current_free_energy,
                                sa_input_dict['distance_to_sec_struct'][sa_input_dict['hx_allowed_bool_seq']])
        corr2 += -corr_coef2[0][1]

        print('heho')

        current_free_energy_fill_min = np.maximum(current_free_energy, 0)
        sec_struct_dist = sa_input_dict['distance_to_sec_struct'][sa_input_dict['hx_allowed_bool_seq']]
        corr_coef = np.corrcoef(current_free_energy_fill_min, sec_struct_dist)
        corr += -corr_coef[0][1]

    return corr


def test_hbond_burial_corr(sa_input_dict):
    state = sa_input_dict['sa_state']
    current_free_energy = np.array(
        [sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state)])

    hbond_free_eng = current_free_energy[
        sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'][
            sa_input_dict['hx_allowed_index']] == 1]

    corr = 0
    corr2 = 0
    if min(hbond_free_eng) == max(hbond_free_eng):
        corr += 0
    else:

        hbond_free_eng_fill_min = np.maximum(hbond_free_eng, 0)
        hbond_burial = sa_input_dict['hbond_class'].hbond_dict['burial'][
                                    (sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'] == 1)
                                    & (sa_input_dict['hx_allowed_bool_seq']) == True]
        hbond_burial_fill_min = np.maximum(hbond_burial, 60)
        corr_coef2 = np.corrcoef(hbond_free_eng_fill_min, hbond_burial_fill_min)
        corr2 += -corr_coef2[0][1]
        corr_coef = np.corrcoef(hbond_free_eng,
                                sa_input_dict['hbond_class'].hbond_dict['burial'][
                                    (sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'] == 1)
                                    & (sa_input_dict['hx_allowed_bool_seq']) == True])
        corr += -corr_coef[0][1]
    return corr


def test_hbond_rank_factor(sa_input_dict):
    state = sa_input_dict['sa_state']
    current_free_energy = np.array(
        [sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state)])

    sort_index = np.argsort(current_free_energy)

    ranks = np.empty(len(current_free_energy), int)
    ranks[sort_index] = np.arange(len(current_free_energy))

    hbond_ranks = ranks[
        sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'][
            sa_input_dict['hx_allowed_index']] == 1]

    hbond_rank_factor = (float(sum(hbond_ranks)) - sa_input_dict['hbond_rank_worst'])/(sa_input_dict['hbond_rank_best'] - sa_input_dict['hbond_rank_worst'])

    return -hbond_rank_factor


def test_top_stdev(sa_input_dict):

    state = sa_input_dict['sa_state']
    current_free_energy = np.array(
        [sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state)])

    sorted_curr_free_energy = sorted(current_free_energy)
    num_10_percent = np.ceil(0.05 * len(current_free_energy))
    sorted_curr_free_energy_top = sorted_curr_free_energy[-int(num_10_percent):]

    stdev_full = np.std(sorted_curr_free_energy_top)

    return stdev_full


def test_comp_data_mse(sa_input_dict):
    """
    compare energy dataset from other technique and add to optimization score
    :return: mean square error between
    """
    state = sa_input_dict['sa_state']
    current_free_energy = np.array(
        [sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state)])

    delta_g_rmse = mean_squared_error(sa_input_dict['comp_data']['deltaG'], current_free_energy)

    return delta_g_rmse


########################################
# old comp data mse
def old_comp_data_prep_sa_input(sa_input, comp_data_fpath, proline_res_ind):
    if comp_data_fpath:
        df = pd.read_csv(comp_data_fpath)
        resnum, deltag_val = df.iloc[:, 0].values, df.iloc[:, 1].values
        res_index = np.subtract(resnum, 1)
        if len(proline_res_ind) == 0:
            res_index_final = res_index
            deltag_val_final = deltag_val
        else:
            res_index_final = []
            deltag_val_final = []
            for ind, (res_ind, deltag_value) in enumerate(zip(res_index, deltag_val)):
                if res_ind not in proline_res_ind:
                    res_index_final.append(res_ind)
                    deltag_val_final.append(deltag_value)
            res_index_final = np.array(res_index_final)
            deltag_val_final = np.array(deltag_val_final)
        sa_input['comp_data'] = dict()
        sa_input['comp_data']['res_index'] = res_index_final
        sa_input['comp_data']['deltaG'] = deltag_val_final
    else:
        sa_input['comp_data'] = None
    return sa_input


def old_comp_data_mse(self):
    """
    compare energy dataset from other technique and add to optimization score
    :return: mean square error between
    """
    current_free_energy = np.array(
        [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

    comp_free_energy_data = []
    comp_free_energy_sa = []

    for ind, delta_g_state in enumerate(current_free_energy):
        for ind2, (res_ind_comp, delta_g_comp) in enumerate(zip(self.sa_input_dict['comp_data']['res_index'],
                                                                self.sa_input_dict['comp_data']['deltaG'])):
            if ind == res_ind_comp:
                comp_free_energy_data.append(delta_g_comp)
                comp_free_energy_sa.append(delta_g_state)
            else:
                comp_free_energy_data.append(self.sa_input_dict['min_comp_free_energy'])
                comp_free_energy_sa.append(delta_g_state)

    comp_free_energy_data = np.array(comp_free_energy_data)
    comp_free_energy_sa = np.array(comp_free_energy_sa)

    delta_g_rmse = mean_squared_error(comp_free_energy_data, comp_free_energy_sa)

    return delta_g_rmse
###################################################


def map_energy_from_anneal_out_state(anneal_out_state, sa_input_dict):
    current_free_energy = np.array([sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(anneal_out_state)])
    out_energy_array = np.zeros(len(sa_input_dict['prot_sequence']))
    out_res_index_array = np.empty(len(sa_input_dict['prot_sequence']))
    out_res_index_array.fill(np.nan)
    out_res_index_array[sa_input_dict['hx_allowed_index']] = anneal_out_state
    out_energy_array[sa_input_dict['hx_allowed_index']] = current_free_energy
    # out_array[sa_input['active_hx_rates'] == 0] = np.nan
    if len(sa_input_dict['cterm']) > 0:
        out_energy_array[-len(sa_input_dict['cterm']):] = np.nan
    if len(sa_input_dict['nterm']) > 0:
        out_energy_array[0:len(sa_input_dict['nterm'])] = np.nan
    return out_energy_array, out_res_index_array


class DeltaGMapping(Annealer):
    """
    Annealing class to map delta G values for each residue
    """

    def __init__(self, sa_input, update_interval=100):

        self.update_interval = update_interval

        state = sa_input['sa_state']

        #randomize state here
        random.shuffle(state)

        self.sa_input_dict = sa_input
        self.weights = sa_input['weights']

        # initialize writing file

        # weights header
        weights_dict_keys = list(self.weights.keys())
        weights_dict_keys_str = ','.join(str(x) for x in weights_dict_keys)
        weights_dict_vals = list(self.weights.values())
        weights_dict_vals_str = ','.join(str(x) for x in weights_dict_vals)

        weights_str = 'Weights_Params\n' + weights_dict_keys_str + '\n' + weights_dict_vals_str + '\n'


        # data header
        data_header = 'step,sa_opt_val,pair_energy_term,full_burial_corr,hbond_burial_corr,hbond_rank_factor,distance_to_nonpolar_res_corr,distance_to_sec_struct_corr,top_stdev,comp_deltag_rmse'

        data_header +='\n'

        out_string = weights_str + '\nSA_Output\n' + data_header

        fpath = os.path.join(sa_input['output_dir'], sa_input['pdb_fname'] + '_deltaG_traj_' + sa_input['outfile_label'] + '.csv')
        with open(fpath, 'w') as outfile:
            outfile.write(out_string)

        super(DeltaGMapping, self).__init__(state)

    def move(self):
        """
        randomly move and swap two states
        :return:
        """
        a = random.randint(0, len(self.state) - 1)
        b = random.randint(0, len(self.state) - 1)
        self.state[a], self.state[b] = self.state[b], self.state[a]

    def energy(self):
        """
        Calculate the energy ... todo: needs better description
        :return: energy
        """

        opt_val = 0

        anneal_class_terms = dict()

        anneal_class_terms['pair_energy'] = self.weights['pair_energy'] * self.pair_energy()
        anneal_class_terms['full_burial_corr'] = self.weights['full_burial'] * self.full_burial_corr()
        anneal_class_terms['hbond_burial_corr'] = self.weights['hbond_burial'] * self.hbond_burial_corr()
        anneal_class_terms['hbond_rank_factor'] = self.weights['hbond_rank'] * self.hbond_rank_fact()
        anneal_class_terms['distance_to_nonpolar_res_corr'] = self.weights[
                                                                  'distance_to_nonpolar_res'] * self.distance_to_nonpolar_res_corr()
        anneal_class_terms['distance_to_sec_struct_corr'] = self.weights[
                                                                'distance_to_sec_struct'] * self.distance_to_sec_struct_corr()
        anneal_class_terms['top_stdev'] = self.weights['top_stdev'] * self.top_stdev_energy()

        if self.sa_input_dict['comp_data']:
            anneal_class_terms['comp_deltaG_rmse_term'] = self.weights['comp_deltag_rmse'] * self.comp_data_mse()

            anneal_class_terms_list = list(anneal_class_terms.values())
            opt_val += sum(anneal_class_terms_list)
        else:
            anneal_class_terms_list = list(anneal_class_terms.values())
            opt_val += sum(anneal_class_terms_list)

            anneal_class_terms['comp_deltaG_rmse_term'] = '-'

        if self.write_updates:
            if self.curr_step % self.update_interval == 0:
                self.write_output_string(self.curr_step, opt_val, anneal_class_terms)

        return opt_val

    def write_output_string(self, step, opt_val, anneal_weights):
        """
        write output for the weights along SA trajectory
        :return:
        """
        fpath = os.path.join(self.sa_input_dict['output_dir'],
                             self.sa_input_dict['pdb_fname'] + '_deltaG_traj_' + self.sa_input_dict['outfile_label'] + '.csv')
        anneal_weights_list = list(anneal_weights.values())
        anneal_weights_str_list = ','.join(str(x) for x in anneal_weights_list)

        # 'sa_opt_val,full_burial_corr,hbond_burial_corr,hbond_rank_factor,distance_to_nonpolar_res_corr,distance_to_sec_struct_corr,top_stdev'

        with open(fpath, 'a') as outfile:
            outfile.write('{},{},{}\n'.format(step, str(opt_val), anneal_weights_str_list))
            outfile.close()


    def map_energy(self):
        current_free_energy = np.array([self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])
        out_array = np.zeros(len(self.sa_input_dict['prot_sequence']))
        out_array[self.sa_input_dict['hx_allowed_index']] = current_free_energy
        # out_array[sa_input['active_hx_rates'] == 0] = np.nan
        if len(self.sa_input_dict['cterm']) > 0:
            out_array[-len(self.sa_input_dict['cterm']):] = -2
        if len(self.sa_input_dict['nterm']) > 0:
            out_array[0:len(self.sa_input_dict['nterm'])] = -2
        return out_array

    def pair_energy(self):
        pair_e = []
        for aa1, aa2 in zip(self.sa_input_dict['active_res_set_1'], self.sa_input_dict['active_res_set_2']):
            eng = self.sa_input_dict['pair_energies_multi_grid'][aa1, aa2, self.state[aa1], self.state[aa2]]
            pair_e.append(eng)
        avg_pair_e = np.average(pair_e)
        return avg_pair_e

    def comp_data_mse(self):
        """
        compare energy dataset from other technique and add to optimization score
        :return: mean square error between
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        delta_g_rmse = mean_squared_error(self.sa_input_dict['comp_data']['deltaG'], current_free_energy)

        return delta_g_rmse

    def full_burial_corr(self):
        """
        calculate the correlation coeffs between an array of free energy grid and burial distribution. Return the Cij
        :return:
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])
        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            current_free_energy_fill_min = np.maximum(current_free_energy, 0)
            burial = self.sa_input_dict['hbond_class'].hbond_dict['burial'][
                self.sa_input_dict['hx_allowed_index']]
            burial_fill_min = np.maximum(burial, 60)
            corr_coef = np.corrcoef(current_free_energy_fill_min, burial_fill_min)
            corr += -corr_coef[0][1]
        return corr

    def hbond_burial_corr(self):
        """
        calculate the correlation coeffs between the burial distance where hbond and hx is allowed and free energy grid.
        Return the Cij
        :return:
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        hbond_free_eng = current_free_energy[
            self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'][
                self.sa_input_dict['hx_allowed_index']] == 1]

        corr = 0
        if min(hbond_free_eng) == max(hbond_free_eng):
            corr += 0
        else:
            hbond_free_eng_fill_min = np.maximum(hbond_free_eng, 0)
            hbond_burial = self.sa_input_dict['hbond_class'].hbond_dict['burial'][
                (self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'] == 1)
                & (self.sa_input_dict['hx_allowed_bool_seq']) == True]
            hbond_burial_fill_min = np.maximum(hbond_burial, 60)
            corr_coef = np.corrcoef(hbond_free_eng_fill_min, hbond_burial_fill_min)
            corr += -corr_coef[0][1]
        return corr

    def distance_to_nonpolar_res_corr(self):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            current_free_energy_fill_min = np.maximum(current_free_energy, 0)
            dist_non_polar_res_arr = self.sa_input_dict['distance_to_nonpolar_res'][self.sa_input_dict['hx_allowed_bool_seq']]
            corr_coef = np.corrcoef(current_free_energy_fill_min, dist_non_polar_res_arr)
            corr += -corr_coef[0][1]
        return corr

    def distance_to_sec_struct_corr(self):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            current_free_energy_fill_min = np.maximum(current_free_energy, 0)
            dist_sec_struct_arr = self.sa_input_dict['distance_to_sec_struct'][self.sa_input_dict['hx_allowed_bool_seq']]
            corr_coef = np.corrcoef(current_free_energy_fill_min, dist_sec_struct_arr)
            corr += -corr_coef[0][1]
        return corr

    def hbond_rank_fact(self):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        sort_index = np.argsort(current_free_energy)

        ranks = np.empty(len(current_free_energy), int)
        ranks[sort_index] = np.arange(len(current_free_energy))

        hbond_ranks = ranks[
            self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'][
                self.sa_input_dict['hx_allowed_index']] == 1]

        hbond_rank_factor = (float(sum(hbond_ranks)) - self.sa_input_dict['hbond_rank_worst']) / (
                    self.sa_input_dict['hbond_rank_best'] - self.sa_input_dict['hbond_rank_worst'])

        return -hbond_rank_factor


    def top_stdev_energy(self):
        """
        top 5% of energy stdev
        :param sa_input_dict:
        :return:
        """

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        sorted_curr_free_energy = sorted(current_free_energy)
        num_k_percent = np.ceil(0.05 * len(current_free_energy))
        sorted_curr_free_energy_top = sorted_curr_free_energy[-int(num_k_percent):]

        stdev_full = np.std(sorted_curr_free_energy_top)

        return stdev_full

#decrepate this version
class DeltaGMapping_old(Annealer):
    """
    Annealing class to map delta G values for each residue
    """

    def __init__(self, sa_input, update_interval=100):

        self.update_interval = update_interval

        state = sa_input['sa_state']

        #randomize state here
        random.shuffle(state)

        self.sa_input_dict = sa_input
        self.weights = sa_input['weights']

        # initialize writing file

        # weights header
        weights_dict_keys = list(self.weights.keys())
        weights_dict_keys_str = ','.join(str(x) for x in weights_dict_keys)
        weights_dict_vals = list(self.weights.values())
        weights_dict_vals_str = ','.join(str(x) for x in weights_dict_vals)

        weights_str = 'Weights_Params\n' +  weights_dict_keys_str + '\n' + weights_dict_vals_str + '\n'


        # data header
        data_header = 'step,sa_opt_val,pair_energy_term,full_burial_corr,hbond_burial_corr,hbond_rank_factor,distance_to_nonpolar_res_corr,distance_to_sec_struct_corr,top_stdev,comp_deltag_rmse'

        data_header +='\n'

        out_string = weights_str + '\nSA_Output\n' + data_header

        fpath = os.path.join(sa_input['output_dir'], sa_input['pdb_fname'] + '_deltaG_traj_' + sa_input['outfile_label'] + '.csv')
        with open(fpath, 'w') as outfile:
            outfile.write(out_string)

        super(DeltaGMapping_old, self).__init__(state)

    def move(self):
        """
        randomly move and swap two states
        :return:
        """
        a = random.randint(0, len(self.state) - 1)
        b = random.randint(0, len(self.state) - 1)
        self.state[a], self.state[b] = self.state[b], self.state[a]

    def energy(self):
        """
        Calculate the energy ... todo: needs better description
        :return: energy
        """

        opt_val = 0

        anneal_class_terms = dict()

        anneal_class_terms['pair_energy'] = self.weights['pair_energy'] * self.pair_energy()
        anneal_class_terms['full_burial_corr'] = self.weights['full_burial'] * self.full_burial_corr()
        anneal_class_terms['hbond_burial_corr'] = self.weights['hbond_burial'] * self.hbond_burial_corr()
        anneal_class_terms['hbond_rank_factor'] = self.weights['hbond_rank'] * self.hbond_rank_fact()
        anneal_class_terms['distance_to_nonpolar_res_corr'] = self.weights[
                                                                  'distance_to_nonpolar_res'] * self.distance_to_nonpolar_res_corr()
        anneal_class_terms['distance_to_sec_struct_corr'] = self.weights[
                                                                'distance_to_sec_struct'] * self.distance_to_sec_struct_corr()
        anneal_class_terms['top_stdev'] = self.weights['top_stdev'] * self.top_stdev_energy()

        if self.sa_input_dict['comp_data']:
            anneal_class_terms['comp_deltaG_rmse_term'] = self.weights['comp_deltag_rmse'] * self.comp_data_mse()

            anneal_class_terms_list = list(anneal_class_terms.values())
            opt_val += sum(anneal_class_terms_list)
        else:
            anneal_class_terms_list = list(anneal_class_terms.values())
            opt_val += sum(anneal_class_terms_list)

            anneal_class_terms['comp_deltaG_rmse_term'] = '-'

        if self.write_updates:
            if self.curr_step % self.update_interval == 0:
                self.write_output_string(self.curr_step, opt_val, anneal_class_terms)

        return opt_val

    def write_output_string(self, step, opt_val, anneal_weights):
        """
        write output for the weights along SA trajectory
        :return:
        """
        fpath = os.path.join(self.sa_input_dict['output_dir'],
                             self.sa_input_dict['pdb_fname'] + '_deltaG_traj_' + self.sa_input_dict['outfile_label'] + '.csv')
        anneal_weights_list = list(anneal_weights.values())
        anneal_weights_str_list = ','.join(str(x) for x in anneal_weights_list)

        # 'sa_opt_val,full_burial_corr,hbond_burial_corr,hbond_rank_factor,distance_to_nonpolar_res_corr,distance_to_sec_struct_corr,top_stdev'

        with open(fpath, 'a') as outfile:
            outfile.write('{},{},{}\n'.format(step, str(opt_val), anneal_weights_str_list))
            outfile.close()


    def map_energy(self):
        current_free_energy = np.array([self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])
        out_array = np.zeros(len(self.sa_input_dict['prot_sequence']))
        out_array[self.sa_input_dict['hx_allowed_index']] = current_free_energy
        # out_array[sa_input['active_hx_rates'] == 0] = np.nan
        if len(self.sa_input_dict['cterm']) > 0:
            out_array[-len(self.sa_input_dict['cterm']):] = -2
        if len(self.sa_input_dict['nterm']) > 0:
            out_array[0:len(self.sa_input_dict['nterm'])] = -2
        return out_array

    def pair_energy(self):

        pair_e = 0
        for aa1, aa2 in zip(self.sa_input_dict['active_res_set_1'], self.sa_input_dict['active_res_set_2']):
            eng = self.sa_input_dict['pair_energies_multi_grid'][aa1, aa2, self.state[aa1], self.state[aa2]]
            pair_e += eng
        return pair_e


    def comp_data_mse(self):
        """
        compare energy dataset from other technique and add to optimization score
        :return: mean square error between
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        delta_g_rmse = mean_squared_error(self.sa_input_dict['comp_data']['deltaG'], current_free_energy)

        return delta_g_rmse


    def full_burial_corr(self):
        """
        calculate the correlation coeffs between an array of free energy grid and burial distribution. Return the Cij
        :return:
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])
        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            corr_coef = np.corrcoef(current_free_energy, self.sa_input_dict['hbond_class'].hbond_dict['burial'][
                self.sa_input_dict['hx_allowed_index']])
            corr += -corr_coef[0][1]
        return corr

    def hbond_burial_corr(self):
        """
        calculate the correlation coeffs between the burial distance where hbond and hx is allowed and free energy grid.
        Return the Cij
        :return:
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        hbond_free_eng = current_free_energy[
            self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'][
                self.sa_input_dict['hx_allowed_index']] == 1]

        corr = 0
        if min(hbond_free_eng) == max(hbond_free_eng):
            corr += 0
        else:
            corr_coef = np.corrcoef(hbond_free_eng,
                                    self.sa_input_dict['hbond_class'].hbond_dict['burial'][
                                        (self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'] == 1)
                                        & (self.sa_input_dict['hx_allowed_bool_seq']) == True])
            corr += -corr_coef[0][1]
        return corr

    def distance_to_nonpolar_res_corr(self):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            corr_coef = np.corrcoef(current_free_energy,
                                    self.sa_input_dict['distance_to_nonpolar_res'][self.sa_input_dict['hx_allowed_bool_seq']])
            corr += -corr_coef[0][1]
        return corr

    def distance_to_sec_struct_corr(self):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            corr_coef = np.corrcoef(current_free_energy,
                                    self.sa_input_dict['distance_to_sec_struct'][self.sa_input_dict['hx_allowed_bool_seq']])
            corr += -corr_coef[0][1]
        return corr

    def hbond_rank_fact(self):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        sort_index = np.argsort(current_free_energy)

        ranks = np.empty(len(current_free_energy), int)
        ranks[sort_index] = np.arange(len(current_free_energy))

        hbond_ranks = ranks[
            self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'][
                self.sa_input_dict['hx_allowed_index']] == 1]

        hbond_ranks_norm = sum(
            np.arange(
                self.sa_input_dict['len_hx_allowed'] - sum(
                    self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list']),
                self.sa_input_dict['len_hx_allowed']))

        hbond_rank_factor = float(sum(hbond_ranks)) / hbond_ranks_norm

        return -hbond_rank_factor


    def top_stdev_energy(self):
        """
        top 5% of energy stdev
        :param sa_input_dict:
        :return:
        """

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        sorted_curr_free_energy = sorted(current_free_energy)
        num_k_percent = np.ceil(0.05 * len(current_free_energy))
        sorted_curr_free_energy_top = sorted_curr_free_energy[-int(num_k_percent):]

        stdev_full = np.std(sorted_curr_free_energy_top)

        return stdev_full



class DeltaGMapping_CSA(object):
    """
    coupled simulated annealeing method for mapping delta G from hx rates.

    The following description taken from csa algorithm (https://github.com/structurely/csa/blob/dev/csa/algorithm.py):

    Interface for performing coupled simulated annealing.
    **Parameters**:
      - target_function: function
            A function which outputs a float.
      - probe_function: function
            a function which will randomly "probe"
            out from the current state, i.e. it will randomly adjust the input
            parameters for the `target_function`.
      - n_annealers: int
            The number of annealing processes to run.
      - initial_state: list
            A list of objects of length `n_probes`.
            This is used to set the initial values of the input parameters for
            `target_function` for all `n_probes` annealing processes.
      - steps: int
            The total number of annealing steps.
      - update_interval: int
            Specifies how many steps in between updates
            to the generation and acceptance temperatures.
      - tgen_initial: float
            The initial value of the generation temperature.
      - tgen_schedule: float
            Determines the factor that tgen is multiplied by during each update.
      - tacc_initial: float
            The initial value of the acceptance temperature.
      - tacc_schedule: float
            Determines the factor that `tacc` is multiplied by during each update.
      - desired_variance: float
            The desired variance of the acceptance probabilities. If not specified,
            `desired_variance` will be set to
            :math:`0.99 * (\\text{max variance}) = 0.99 * \\frac{(m - 1)}{m^2}`,
            where m is the number of annealing processes.
      - verbose: int
            Set verbose=2, 1, or 0 depending on how much output you wish to see
            (2 being the most, 0 being no output).
      - processes: int
            The number of parallel processes. Defaults to a single process.
            If `processes` <= 0, then the number of processes will be set to the
            number of available CPUs. Note that this is different from the
            `n_annealers`. If `target_function` is costly to compute, it might
            make sense to set `n_annealers` = `processes` = max number of CPUs.
            On the other hand, if `target_function` is easy to compute, then the
            CSA process will likely run a LOT faster with a single process due
            to the overhead of using multiple processes.


    """

    def __init__(self, sa_input,
                 n_annealers=10,
                 steps=10000,
                 update_interval=5,
                 tgen_initial=0.01,
                 tgen_schedule=0.99999,
                 tacc_initial=25000,
                 tacc_low=2.5,
                 tacc_schedule=0.98,
                 desired_variance=None,
                 verbose=1,
                 processes=1):
        """
        initialize object. needs the sa input dictionary and number of annealing method to run independently
        :param sa_input: sa_input dictionary output from prep_sa_input
        :param n_annealers: int. number of annealers to run independently
        """

        t_min = 2.5

        self.sa_input_dict = sa_input
        # self.target_function = target_function
        # self.probe_function = probe_function
        self.n_annealers = n_annealers
        self.steps = steps
        self.update_interval = update_interval
        self.tgen_initial = tgen_initial
        self.tgen_schedule = tgen_schedule
        self.tacc_initial = tacc_initial
        self.tacc_low = tacc_low
        self.tacc_schedule = tacc_schedule
        self.desired_variance = desired_variance
        self.verbose = verbose
        self.processes = processes

        # weights
        self.weights = sa_input['weights']

        # initializing states
        state = sa_input['sa_state']
        init_state = []
        for num in range(n_annealers):
            random.shuffle(state)
            state_new = state.copy()
            init_state.append(state_new)

        self.init_state = init_state

        # initialize writing file

        # weights header
        weights_dict_keys = list(self.weights.keys())
        weights_dict_keys_str = ','.join(str(x) for x in weights_dict_keys)
        weights_dict_vals = list(self.weights.values())
        weights_dict_vals_str = ','.join(str(x) for x in weights_dict_vals)

        weights_str = 'Weights_Params\n' + weights_dict_keys_str + '\n' + weights_dict_vals_str + '\n'

        # data header
        data_header = 'step,proc_term,acc_temp,gen_temp,sa_opt_val,pair_energy_term,full_burial_corr,hbond_burial_corr,hbond_rank_factor,distance_to_nonpolar_res_corr,distance_to_sec_struct_corr,top_stdev,comp_deltag_rmse'

        data_header += '\n'

        out_string = weights_str + '\nSA_Output\n' + data_header

        fpath = os.path.join(sa_input['output_dir'],
                             sa_input['pdb_fname'] + '_deltaG_traj_' + sa_input['outfile_label'] + '.csv')

        self.update_fpath = fpath

        with open(self.update_fpath, 'w') as outfile:
            outfile.write(out_string)


    def anneal_optimize(self):
        """
        coupled simanneal method.
        :return:
        """
        annealer = CoupledAnnealer(target_function=self.optimize_val,
                                   probe_function=self.probe_states,
                                   update_filepath=self.update_fpath,
                                   n_annealers=self.n_annealers,
                                   initial_state=self.init_state,
                                   steps=self.steps,
                                   update_interval=self.update_interval,
                                   tgen_initial=self.tgen_initial,
                                   tgen_schedule=self.tgen_schedule,
                                   tacc_initial=self.tacc_initial,
                                   tacc_low=self.tacc_low,
                                   tacc_schedule=self.tacc_schedule,
                                   desired_variance=self.desired_variance,
                                   verbose=self.verbose,
                                   processes=self.processes)
        annealer.anneal()

        opt_energy, opt_state = annealer.get_best()
        return opt_state, opt_energy


    def probe_states(self, state_, tgen):
        """
        tgen is ignored.
        :return:
        """
        a = random.randint(0, len(state_) - 1)
        b = random.randint(0, len(state_) - 1)
        state_[a], state_[b] = state_[b], state_[a]
        return state_

    def optimize_val(self, state_):
        """

        :param state_:
        :return:
        """

        opt_val = 0

        anneal_class_terms = dict()

        anneal_class_terms['pair_energy'] = self.weights['pair_energy'] * self.pair_energy(state_)
        anneal_class_terms['full_burial_corr'] = self.weights['full_burial'] * self.full_burial_corr(state_)
        anneal_class_terms['hbond_burial_corr'] = self.weights['hbond_burial'] * self.hbond_burial_corr(state_)
        anneal_class_terms['hbond_rank_factor'] = self.weights['hbond_rank'] * self.hbond_rank_fact(state_)
        anneal_class_terms['distance_to_nonpolar_res_corr'] = self.weights[
                                                                  'distance_to_nonpolar_res'] * self.distance_to_nonpolar_res_corr(state_)
        anneal_class_terms['distance_to_sec_struct_corr'] = self.weights[
                                                                'distance_to_sec_struct'] * self.distance_to_sec_struct_corr(state_)
        anneal_class_terms['top_stdev'] = self.weights['top_stdev'] * self.top_stdev_energy(state_)

        if self.sa_input_dict['comp_data']:
            anneal_class_terms['comp_deltaG_rmse_term'] = self.weights['comp_deltag_rmse'] * self.comp_data_mse(state_)

            anneal_class_terms_list = list(anneal_class_terms.values())
            opt_val += sum(anneal_class_terms_list)
        else:
            anneal_class_terms_list = list(anneal_class_terms.values())
            opt_val += sum(anneal_class_terms_list)

            anneal_class_terms['comp_deltaG_rmse_term'] = '-'

        # self.write_output_string(opt_val, anneal_class_terms)

        return opt_val, anneal_class_terms

    def map_energy(self):
        current_free_energy = np.array([self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])
        out_array = np.zeros(len(self.sa_input_dict['prot_sequence']))
        out_array[self.sa_input_dict['hx_allowed_index']] = current_free_energy
        # out_array[sa_input['active_hx_rates'] == 0] = np.nan
        if len(self.sa_input_dict['cterm']) > 0:
            out_array[-len(self.sa_input_dict['cterm']):] = -2
        if len(self.sa_input_dict['nterm']) > 0:
            out_array[0:len(self.sa_input_dict['nterm'])] = -2
        return out_array

    def pair_energy(self, state_):
        pair_e = 0
        for aa1, aa2 in zip(self.sa_input_dict['active_res_set_1'], self.sa_input_dict['active_res_set_2']):
            eng = self.sa_input_dict['pair_energies_multi_grid'][aa1, aa2, state_[aa1], state_[aa2]]
            pair_e += eng
        return pair_e


    def comp_data_mse(self, state_):
        """
        compare energy dataset from other technique and add to optimization score
        :return: mean square error between
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(self.state)])

        delta_g_rmse = mean_squared_error(self.sa_input_dict['comp_data']['deltaG'], current_free_energy)

        return delta_g_rmse


    def full_burial_corr(self, state_):
        """
        calculate the correlation coeffs between an array of free energy grid and burial distribution. Return the Cij
        :return:
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state_)])
        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            corr_coef = np.corrcoef(current_free_energy, self.sa_input_dict['hbond_class'].hbond_dict['burial'][
                self.sa_input_dict['hx_allowed_index']])
            corr += -corr_coef[0][1]
        return corr

    def hbond_burial_corr(self, state_):
        """
        calculate the correlation coeffs between the burial distance where hbond and hx is allowed and free energy grid.
        Return the Cij
        :return:
        """
        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state_)])

        hbond_free_eng = current_free_energy[
            self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'][
                self.sa_input_dict['hx_allowed_index']] == 1]

        corr = 0
        if min(hbond_free_eng) == max(hbond_free_eng):
            corr += 0
        else:
            comp_arr = self.sa_input_dict['hbond_class'].hbond_dict['burial'][
                                        (self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'] == 1)
                                        & (self.sa_input_dict['hx_allowed_bool_seq']) == True]
            corr_coef = np.corrcoef(hbond_free_eng,
                                    self.sa_input_dict['hbond_class'].hbond_dict['burial'][
                                        (self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'] == 1)
                                        & (self.sa_input_dict['hx_allowed_bool_seq']) == True])
            corr += -corr_coef[0][1]
        return corr

    def distance_to_nonpolar_res_corr(self, state_):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state_)])

        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            corr_coef = np.corrcoef(current_free_energy,
                                    self.sa_input_dict['distance_to_nonpolar_res'][self.sa_input_dict['hx_allowed_bool_seq']])
            corr += -corr_coef[0][1]
        return corr

    def distance_to_sec_struct_corr(self, state_):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state_)])

        corr = 0
        if min(current_free_energy) == max(current_free_energy):
            corr += 0
        else:
            corr_coef = np.corrcoef(current_free_energy,
                                    self.sa_input_dict['distance_to_sec_struct'][self.sa_input_dict['hx_allowed_bool_seq']])
            corr += -corr_coef[0][1]
        return corr

    def hbond_rank_fact(self, state_):

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state_)])

        sort_index = np.argsort(current_free_energy)

        ranks = np.empty(len(current_free_energy), int)
        ranks[sort_index] = np.arange(len(current_free_energy))

        hbond_ranks = ranks[
            self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list'][
                self.sa_input_dict['hx_allowed_index']] == 1]

        hbond_ranks_norm = sum(
            np.arange(
                self.sa_input_dict['len_hx_allowed'] - sum(
                    self.sa_input_dict['hbond_class'].hbond_dict['hbond_bool_num_list']),
                self.sa_input_dict['len_hx_allowed']))

        hbond_rank_factor = float(sum(hbond_ranks)) / hbond_ranks_norm

        return -hbond_rank_factor

    def top_stdev_energy(self, state_):
        """
        top 5% of energy stdev
        :param sa_input_dict:
        :return:
        """

        current_free_energy = np.array(
            [self.sa_input_dict['free_energy_grid'][ind][eng] for ind, eng in enumerate(state_)])

        sorted_curr_free_energy = sorted(current_free_energy)
        num_k_percent = np.ceil(0.05 * len(current_free_energy))
        sorted_curr_free_energy_top = sorted_curr_free_energy[-int(num_k_percent):]

        stdev_full = np.std(sorted_curr_free_energy_top)

        return stdev_full


def write_output_delta_g_map(pdb_fpath, output_dirpath, outfile_label, energy, mapped_energy, sa_out_state,
                             raw_sequence_num_array, anneal_class_terms_dict, sa_params_dict):
    """
    writes the output of delta g mapping to a csv file
    :param output_dirpath: output directory path
    :param energy: annealed energy
    :param mapped_energy: mapped energy to residues
    :return: None. Saves the file
    """

    string1 = '#SA_OUT\n'

    sa_param_str = ''
    for sa_key, sa_val in sa_params_dict.items():
        sa_param_str += '{},{}\n'.format(sa_key, sa_val)

    energy_string = '\nEnergy\n'+str(energy)+'\n'


    header1_string = '\n#Anneal_Class_Terms\n'

    anneal_header_str = 'terms,wt,fun,wt_fun\n'
    anneal_values_str = ''
    for dict_key, dict_item in anneal_class_terms_dict.items():
        item_str = ','.join(str(x) for x in dict_item)
        anneal_values_str += '{},{}\n'.format(dict_key, item_str)


    string2 = '\n#DeltaG_Output\n'
    header_string = 'sa_out_state,num,raw_seq_num,mapped_energy\n'
    data_string = ''
    for index, (sa_state, map_e, raw_seq_resnum) in enumerate(zip(sa_out_state, mapped_energy, raw_sequence_num_array)):
        data_string += '{},{},{},{}\n'.format(str(sa_state), str(index+1), str(raw_seq_resnum), str(map_e))

    out_string = string1 + sa_param_str + energy_string + header1_string + anneal_header_str + anneal_values_str + string2 + \
                 header_string + data_string

    pdb_path, pdb_fname = os.path.split(pdb_fpath)
    out_fname = pdb_fname + '_hx_rate_deltaG_' + outfile_label + '.csv'

    with open(os.path.join(output_dirpath, out_fname), 'w') as outfile:
        outfile.write(out_string)
        outfile.close()


def plot_energy_in_decreasing_order(mapped_energy):

    sort_map_energy = sorted(mapped_energy, reverse=True)
    sort_map_final = []
    for item in sort_map_energy:
        if item !=0:
            if item !=-2:
                sort_map_final.append(item)

    sort_map_final = np.array(sort_map_final)

    plt.scatter(np.arange(len(sort_map_final)), sort_map_final)


def calculate_anneal_class_terms(anneal_copy, route, sa_energy_weights_dict, csa=False, comp_data_rmse=False):
    """
    calculate anneal class terms
    :param anneal_copy: deep copy of annealer
    :param best_route: best route output from simanneal
    :param sa_energy_weight_dict: sa energy weight dictionary
    :return:
    """

    if csa:
        pair_energy = anneal_copy.pair_energy(route)
        full_burial_corr = anneal_copy.full_burial_corr(route)
        hbond_burial_corr = anneal_copy.hbond_burial_corr(route)
        hbond_rank_corr = anneal_copy.hbond_rank_fact(route)
        distance_to_nonpolar_res_corr = anneal_copy.distance_to_nonpolar_res_corr(route)
        distance_to_sec_struct_corr = anneal_copy.distance_to_sec_struct_corr(route)
        top_stdev = anneal_copy.top_stdev_energy(route)
        if comp_data_rmse:
            comp_deltag_rmse = anneal_copy.comp_data_mse(route)
        else:
            comp_deltag_rmse = '-'
    else:
        pair_energy = anneal_copy.pair_energy()
        full_burial_corr = anneal_copy.full_burial_corr()
        hbond_burial_corr = anneal_copy.hbond_burial_corr()
        hbond_rank_corr = anneal_copy.hbond_rank_fact()
        distance_to_nonpolar_res_corr = anneal_copy.distance_to_nonpolar_res_corr()
        distance_to_sec_struct_corr = anneal_copy.distance_to_sec_struct_corr()
        top_stdev = anneal_copy.top_stdev_energy()
        if comp_data_rmse:
            comp_deltag_rmse = anneal_copy.comp_data_mse()
        else:
            comp_deltag_rmse = '-'

    anneal_class_terms = dict()
    anneal_class_terms['pair_energy'] = [sa_energy_weights_dict['pair_energy'], pair_energy,
                                         sa_energy_weights_dict['pair_energy'] * pair_energy]
    anneal_class_terms['full_burial'] = [sa_energy_weights_dict['full_burial'], full_burial_corr,
                                         sa_energy_weights_dict['full_burial'] * full_burial_corr]
    anneal_class_terms['hbond_burial'] = [sa_energy_weights_dict['hbond_burial'], hbond_burial_corr,
                                          sa_energy_weights_dict['hbond_burial'] * hbond_burial_corr]
    anneal_class_terms['hbond_rank'] = [sa_energy_weights_dict['hbond_rank'], hbond_rank_corr,
                                        sa_energy_weights_dict['hbond_rank'] * hbond_rank_corr]
    anneal_class_terms['distance_to_nonpolar_res'] = [sa_energy_weights_dict['distance_to_nonpolar_res'],
                                                      distance_to_nonpolar_res_corr,
                                                      sa_energy_weights_dict['distance_to_nonpolar_res'] * distance_to_nonpolar_res_corr]
    anneal_class_terms['distance_to_sec_struct'] = [sa_energy_weights_dict['distance_to_sec_struct'],
                                                    distance_to_sec_struct_corr,
                                                    sa_energy_weights_dict['distance_to_sec_struct'] * distance_to_sec_struct_corr]
    anneal_class_terms['top_stdev'] = [sa_energy_weights_dict['top_stdev'], top_stdev,
                                       sa_energy_weights_dict['top_stdev'] * top_stdev]
    if comp_data_rmse:
        anneal_class_terms['comp_deltag_rmse'] = [sa_energy_weights_dict['comp_deltag_rmse'], comp_deltag_rmse,
                                                  sa_energy_weights_dict['comp_deltag_rmse'] * comp_deltag_rmse]
    else:
        anneal_class_terms['comp_deltag_rmse'] = [sa_energy_weights_dict['comp_deltag_rmse'], comp_deltag_rmse,
                                                  comp_deltag_rmse]

    return anneal_class_terms

# @profile
def delta_g_mapping(pdb_fpath, hx_rate_fpath, output_dirpath, delta_g_interpol_fpath=None, outfile_label='01', comp_data_fpath=None, nterm='',
                    cterm='', hbond_length=2.7, hbond_angle=120, min_free_energy=0, temp=295, r_constant=1.9872036e-3,
                    pH=6.15, net_charge_corr=False, sa_energy_weights=None, simanneal_steps=1000, ssa_minutes=5,
                    csa=False, n_annealers=20):
    """
    use the DeltaGMapping class to determine delta G for each residue position
    :param pdb_fpath: pdb filepath
    :param hx_rate_fpath: hx rate file path
    :param output_dirpath: output directory
    :param nterm: nterm addition to sequence
    :param cterm: cterm addition to sequence
    :param temp: temperature for free energy calculation from hx rates
    :param simanneal_steps: simanneal steps
    :param simanneal_update: simanneal update frequence
    :return: state_order, energy, mapped_energy
    """
    # this file path is hardcoded. Its a file generated by Gabe that is used to calculate free energy between pairs.
    # Its an interpolation object that was generated using some standard protein structure database
    if delta_g_interpol_fpath:
        deltag_interpol_fpath = delta_g_interpol_fpath
    else:
        deltag_interpol_fpath = "input-file-examples/newrect.pickle"

    sa_input = prep_sa_input(deltag_interpol_fpath, hx_rate_fpath, pdb_fpath, output_dirpath,
                             outfile_label=outfile_label, comp_data_fpath=comp_data_fpath,
                             nterm=nterm, cterm=cterm, hbond_length=hbond_length, hbond_angle=hbond_angle,
                             min_free_energy=min_free_energy, temp=temp, r_constant=r_constant,
                             sa_energy_weights=sa_energy_weights, pH=pH, net_charge_corr=net_charge_corr)

    if csa:
        ds = DeltaGMapping_CSA(sa_input, n_annealers=n_annealers, steps=simanneal_steps,
                               processes=1, verbose=1)
        route, energy_ = ds.anneal_optimize()
        annealer_copy = copy.deepcopy(ds)
        tmin = annealer_copy.tacc_low
        tmax = annealer_copy.tacc_initial
        sa_steps = annealer_copy.steps
    else:
        deltag_anneal = DeltaGMapping(sa_input)
        auto_schedule = deltag_anneal.auto(minutes=ssa_minutes)
        print(auto_schedule)
        deltag_anneal.set_schedule(auto_schedule)
        # deltag_anneal.steps = simanneal_steps
        # deltag_anneal.steps = 300

        deltag_anneal.copy_strategy = "slice"
        route, energy_ = deltag_anneal.anneal()
        annealer_copy = copy.deepcopy(deltag_anneal)
        tmin = deltag_anneal.Tmin
        tmax = deltag_anneal.Tmax
        sa_steps = deltag_anneal.steps

    sa_params = dict()
    sa_params['Tmin'] = tmin
    sa_params['Tmax'] = tmax
    sa_params['steps'] = sa_steps

    best_anneal_class_terms = calculate_anneal_class_terms(annealer_copy, route, sa_input['weights'],
                                                           comp_data_rmse=comp_data_fpath, csa=csa)

    mapped_energy, mapped_res_index = map_energy_from_anneal_out_state(route, sa_input)

    write_output_delta_g_map(pdb_fpath, output_dirpath, outfile_label, energy_, mapped_energy, mapped_res_index,
                             sa_input['raw_seq_num_array'], best_anneal_class_terms, sa_params)

    return route, energy_, mapped_energy

def parser_commands():
    """
    simple parser function. only needs the sa param file as input
    :return: parser
    """
    parser = optparse.OptionParser(description='Run SA/CSA using the params defined in the param file (csv)')
    parser.add_option('-i', '--input', dest='input', default='sa_run_params.csv', help='file path for sa param input type .csv')
    return parser

def read_file_gen_dict(filepath):
    """

    :param filepath:
    :return:
    """
    file_dict = dict()
    with open(filepath, 'r') as inputfile:
        param_lines = inputfile.read().splitlines()
        for line in param_lines:
            chars = line.split(',')
            file_dict[chars[0]] = chars[1]
    return file_dict


def run_sa_from_parser(parser):
    """
    read the param file and run the sa/csa
    :return:
    """
    options, args = parser.parse_args()
    input_fpath = options.input

    param_dict = read_file_gen_dict(input_fpath)

    if param_dict['deltag_interpol_fpath'] == '':
        param_dict['deltag_interpol_fpath'] = None

    if param_dict['sa_energy_wt_fpath'] == '':
        param_dict['sa_energy_weights'] = None
    else:
        param_dict['sa_energy_weights'] = read_file_gen_dict(param_dict['sa_energy_wt_fpath'])
        for key, val in param_dict['sa_energy_weights'].items():
            param_dict['sa_energy_weights'][key] = float(val)

    if param_dict['csa'] == str(0):
        param_dict['csa_bool'] = False
    else:
        param_dict['csa_bool'] = True

    if param_dict['comp_data_fpath'] == '':
        param_dict['comp_data_fpath'] = None

    if not os.path.exists(param_dict['outdir_path']):
        os.makedirs(param_dict['outdir_path'])


    for num in range(int(param_dict['n_iter'])):

        delta_g_mapping(delta_g_interpol_fpath=param_dict['deltag_interpol_fpath'],
                        pdb_fpath=param_dict['pdb_fpath'],
                        hx_rate_fpath=param_dict['hx_rate_fpath'],
                        output_dirpath=param_dict['outdir_path'],
                        outfile_label=param_dict['outfile_label']+'_'+str(num+1),
                        comp_data_fpath=param_dict['comp_data_fpath'],
                        nterm=param_dict['nterm'],
                        cterm=param_dict['cterm'],
                        sa_energy_weights=param_dict['sa_energy_weights'],
                        ssa_minutes=int(param_dict['ssa_minutes']),
                        csa=param_dict['csa_bool'])

if __name__=='__main__':

    ###############################################
    # use the following to run using command line
    ###############################
    parser = parser_commands()
    run_sa_from_parser(parser)
    #
    # stop
    ################################################


    # use the following to run by manual inputs
    ##################################################################
    ##################################################################

    # deltag_interpol_fpath = "/Users/smd4193/Documents/hx_ratefit_gabe/refit_new_SA_weights/newrect.pickle"
    # #
    # hx_rate_fpath = "/Users/smd4193/Documents/hx_ratefit_gabe/deltaG_Simmaneal/050420/hx_rate_files/new_mfit_fp3_MES_n_143_HHH_rd4_0518.pdb_fit_hx_rate.csv"
    # #
    # pdb_fpath = "/Users/smd4193/Documents/hx_ratefit_gabe/HHH_rd4_0518.pdb"
    # # #
    # out_dirpath = "/Users/smd4193/Documents/hx_ratefit_gabe/deltaG_Simmaneal/050420/output/EEHEE_rd4_0817_nterm_HM_cterm_GSGS"
    # if not os.path.exists(out_dirpath):
    #     os.makedirs(out_dirpath)
    # #
    # # hx_rate_dict = load_hx_rate_file(hx_rate_fpath)
    # #
    # # comp_data_file = os.path.join(dirpath, "nmr_dG_HHH_rd4_0518.csv")
    # # # comp_data_file = os.path.join(dirpath, "HEEH_rd4_0097_forwardfolding.csv")
    # #
    # # # nmr_df = pd.read_csv(os.path.join(dirpath, "nmr_dG_HHH_rd4_0518.csv"))
    # #
    # # # freenergyfpath = "/Users/smd4193/Documents/hx_ratefit_gabe/for_SA_fit copy/HEEH_rd4_0097_forwardfolding.fes"
    # freenergyfpath = "/Users/smd4193/Documents/hx_ratefit_gabe/nmr_dG_EEHEE_rd4_0871.csv"
    # # # hx_rate_nmr = assemble_hx_rate_from_free_energy_v2(os.path.join(safitdirpath, "HEEH_rd4_0097_forwardfolding.fes"), hx_rate_dict)
    # #
    # #
    # sa_energy_weights_dict = dict()
    # sa_energy_weights_dict['pair_energy'] = 50
    # sa_energy_weights_dict["full_burial"] = 120
    # sa_energy_weights_dict["hbond_burial"] = 14
    # sa_energy_weights_dict["hbond_rank"] = 60
    # sa_energy_weights_dict["distance_to_sec_struct"] = 60  # used to be zero
    # sa_energy_weights_dict["distance_to_nonpolar_res"] = 45  # used to be zero
    # sa_energy_weights_dict["top_stdev"] = 10
    # sa_energy_weights_dict["comp_deltag_rmse"] = 50
    #
    # # sa_input = prep_sa_input(deltag_interpol_fpath, hx_rate_fpath, pdb_fpath, output_dirpath=out_dirpath,
    # #                          outfile_label='test', comp_data_fpath=comp_data_file, hx_rate_from_free_energy=False,
    # #                          nterm='HM', cterm='', sa_energy_weights=sa_energy_weights_dict)
    # #
    # #
    # for num in range(10):
    #
    #     delta_g_mapping(pdb_fpath,
    #                 hx_rate_fpath,
    #                 out_dirpath,
    #                 comp_data_fpath=freenergyfpath,
    #                 nterm='HM',
    #                     cterm='',
    #                 sa_energy_weights=sa_energy_weights_dict,
    #                 ssa_minutes=5,
    #                 outfile_label='0_nmr_'+str(num+1),
    #                     csa=False)


    # ds = DeltaGMapping_CSA(sa_input, n_annealers=20, steps=200000, tacc_initial=25000, processes=1, verbose=1)
    # energy, state = ds.anneal_optimize()
    # mapped_energy, mapped_res_index = map_energy_from_anneal_out_state(state, sa_input)
    # write_output_delta_g_map(pdb_fpath, out_dirpath, 'csa', energy, mapped_energy, mapped_res_index)

    # test_top_stdev(sa_input)
    # test_pair_energy(sa_input)
    # test_full_burial_corr(sa_input)
    # test_hbond_burial_corr(sa_input)
    # test_hbond_rank_factor(sa_input)
    # test_distance_to_nonpolar_res_energy(sa_input)
    # test_distance_to_sec_struct_energy(sa_input)
    # test_hbond_rank_energy(sa_input)
    # test_top_stdev_energy(sa_input)
    # test_pair_energy(sa_input)
    # test_comp_data_mse(sa_input)






# class deltaG_map_hx_rate(object):
#     """
#     Simulated annealing method to estimate delta G unfolding values for residues based on the structure and hx rates
#     """
#
#
