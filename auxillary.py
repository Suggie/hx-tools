"""
Collection of functions and methods to help the main scripts
Has loading files, plotting functions, etc...

Version: 1.1
Author: Suggie
Date:
Location: NU
"""
import numpy as np
from math import sqrt
import pickle
# import elements
import molmass


def load_pickle_file(pickle_fpath):
    """
    loads the pickle file (without any dependence on other classes or objects or functions)
    :param pickle_fpath: file path
    :return: pickle_object
    """
    with open(pickle_fpath, 'rb') as file:
        pk_object = pickle.load(file)
    return pk_object


def smooth_dist_cutoff(x):
    """
    exponential function for smooth continuous function
    :param x: variable
    :return: function output
    """
    return 1./(1. + (np.exp(x-10)))


def calc_rmse(predictions, true_values):
    """
    calculates the root mean square error given the prediction and true values
    :param predictions: array containing predictions
    :param true_values: array containing true values
    :return: root mean square error
    """
    rmse = sqrt(((predictions-true_values)**2).mean())
    return rmse


def hx_active_index_for_sequence(sequence, nterm='', cterm=''):
    """
    :param sequence: sequence string
    :param nterm: nterm amino acid additions (str)
    :param cterm: cterm amino acid additions (str)
    :return: array with index numbers where the sequence is allowed to have hx
    """
    allowed_res_bool = [False for x in nterm] + [False if x == 'P' else True for x in sequence] + [False for x in cterm]
    allowed_res_bool[0:2] = [False, False] # todo: ask gabe: Is this because of adding two amino acids to the n term or always true?

    active_index = list(map(lambda x: 1 if x else 0, allowed_res_bool))
    active_index_sum = np.cumsum(active_index, dtype=object)
    for ind, int in enumerate(active_index):
        # test_seq_res = sequence[ind]
        if int == 0:
            active_index_sum[ind] = None
    active_index_full = []
    for index, item in enumerate(active_index_sum):
        if item != None:
            active_index_full.append(index)
    active_index_full = np.array(active_index_full)
    return active_index_full


def calculate_theoretical_isotope_dist_from_sequence(sequence, num_isotopes=None):
    """
    calculate theoretical isotope distribtuion from a given one letter sequence of protein chain
    :param sequence: sequence in one letter code
    :param num_isotopes: number of isotopes to include. If none, includes all
    :return: isotope distribution
    """
    seq_formula = molmass.Formula(sequence)
    isotope_dist = np.array([x[1] for x in seq_formula.spectrum().values()])
    isotope_dist = isotope_dist/max(isotope_dist)
    if num_isotopes:
        if num_isotopes < len(isotope_dist):
            isotope_dist = isotope_dist[:num_isotopes]
        else:
            fill_arr = np.zeros(num_isotopes - len(isotope_dist))
            isotope_dist = np.append(isotope_dist, fill_arr)
        # isotope_dist = isotope_dist[:num_isotopes]
    return isotope_dist


def calculate_empirical_isotope_dist_from_inegratedt_mz(integrated_mz_intensities, num_isotopes=None):
    """
    calculate the isotope distribution from the integrated mz intensitities
    :param integrated_mz_values: array of integrated mz intensitites
    :param num_isotopes: number of isotopes to include. If none, includes all
    :return: isotope distribution
    """
    isotope_dist = integrated_mz_intensities/max(integrated_mz_intensities)
    if num_isotopes:
        isotope_dist = isotope_dist[:num_isotopes]
    return isotope_dist


def PoiBin(success_probabilities):
    """
    todo: poisson binomial probability distribution function
    :param success_probabilities:
    :return:
    """
    number_trials = success_probabilities.size

    omega = 2 * np.pi / (number_trials + 1)

    chi = np.empty(number_trials + 1, dtype=complex)
    chi[0] = 1
    half_number_trials = int(
        number_trials / 2 + number_trials % 2)
    # set first half of chis:

    # idx_array = np.arange(1, half_number_trials + 1)
    exp_value = np.exp(omega * np.arange(1, half_number_trials + 1) * 1j)
    xy = 1 - success_probabilities + \
         success_probabilities * exp_value[:, np.newaxis]
    # sum over the principal values of the arguments of z:
    argz_sum = np.arctan2(xy.imag, xy.real).sum(axis=1)
    # get d value:
    # exparg = np.log(np.abs(xy)).sum(axis=1)
    d_value = np.exp(np.log(np.abs(xy)).sum(axis=1))
    # get chi values:
    chi[1:half_number_trials + 1] = d_value * np.exp(argz_sum * 1j)

    # set second half of chis:
    chi[half_number_trials + 1:number_trials + 1] = np.conjugate(
        chi[1:number_trials - half_number_trials + 1][::-1])
    chi /= number_trials + 1
    xi = np.fft.fft(chi)
    return xi.real


if __name__=='__main__':
    sequence = 'DLRKNNKELWLLRKNN'
    calculate_theoretical_isotope_dist_from_sequence(sequence)